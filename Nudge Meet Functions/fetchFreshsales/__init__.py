import logging
import psycopg2
import json
import jwt
import os
import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userId = req.params.get('id')
    email = req.params.get('email')

    # conn = createDBConnection('auth', '')
    # cursor = conn.cursor()

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE email='{}';".format(email))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     dbName = records[0]
    # cursor.close()
    # conn.close()
    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    conn = createDBConnection('', dbName)
    cursor = conn.cursor()

    if req.params.get('type') == None:
        cursor.execute(
            "SELECT domain_url, token, freshsales_id FROM freshsales_credentials WHERE leadownerid='" + userId + "'")

        records = cursor.fetchall()

        data = {
            'domain': records[0][0],
            'token': records[0][1],
            'freshsales_id': records[0][2]
        }
    elif req.params.get('type').lower() == 'check':
        cursor.execute("SELECT 'Google' as app FROM google_oauth_tokens WHERE user_id='" + userId +
                       "' UNION ALL SELECT 'Freshsales' as app FROM freshsales_credentials WHERE leadownerid='" + userId + "';")
        records = cursor.fetchall()
        if len(records) == 2:
            data = ['Google', 'Freshsales']
        elif len(records) == 1:
            data = [records[0][0]]
        else:
            data = []
    elif req.params.get('type').lower() == 'signout':
        if req.params.get('app').lower() == 'google':
            cursor.execute(
                "DELETE FROM google_oauth_tokens WHERE user_id = '" + userId + "';")
        elif req.params.get('app').lower() == 'freshsales':
            cursor.execute(
                "DELETE FROM freshsales_credentials WHERE leadownerid = '" + userId + "';")

        conn.commit()
        data = {
            "status": 'success'
        }

    return func.HttpResponse(
        json.dumps(data),
        status_code=200
    )

    # else:
    #     return func.HttpResponse("User not found", status_code=401)
