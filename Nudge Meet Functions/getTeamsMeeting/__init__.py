import logging
import requests
import psycopg2
import json
import jwt

import azure.functions as func


def createDBConnection(dbName):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbName, password, sslmode)
    conn = psycopg2.connect(conn_string)
    logging.info("Connection established")
    return conn


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def fnCheckToken(accessToken, refreshToken):
    returnToken = ""
    try:
        meResp = requests.get("https://graph.microsoft.com/v1.0/me",
                              headers={"Authorization": "Bearer " + accessToken})

        if meResp.status_code == 200:
            returnToken = accessToken
        else:
            returnToken = fnUpdateToken(refreshToken)
    except:
        logging.info('Some error occurred in check token function')
    finally:
        return returnToken


def fnUpdateToken(refreshToken):
    returnToken = ""
    try:
        tokenResp = requests.post("https://login.microsoftonline.com/fb33e7e1-6a98-4d5a-bd25-f47acf95078a/oauth2/v2.0/token",
                                  data={
                                      'client_id': '093c500d-7a6f-449f-8bec-dd4d7dc80322',
                                      'scope': 'openid profile email https://graph.microsoft.com/Calendars.Read https://graph.microsoft.com/Chat.Read https://graph.microsoft.com/Directory.Read.All https://graph.microsoft.com/Mail.Read https://graph.microsoft.com/User.Export.All https://graph.microsoft.com/User.Read https://graph.microsoft.com/User.Read.All https://graph.microsoft.com/User.ReadBasic.All https://graph.microsoft.com/User.ReadWrite.All https://graph.microsoft.com/Files.Read.All https://graph.microsoft.com/Sites.Read.All https://graph.microsoft.com/Files.ReadWrite',
                                      'refresh_token': refreshToken,
                                      'grant_type': 'refresh_token'
                                  })
        if tokenResp.status_code == 200:
            returnToken = json.loads(tokenResp.text)["access_token"]
    except:
        logging.info("Some error occurred in update token function")
    finally:
        return returnToken


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    db = 'presales_prod'
    conn = createDBConnection(dbName=db)
    cursor = conn.cursor()

    cursor.execute(
        f"SELECT access_token, refresh_token FROM oauth_tokens WHERE token_type='azure';")

    azureRecords = cursor.fetchall()

    for x in range(0, len(azureRecords)):
        tokenData = {
            'access_token': azureRecords[x][0],
            'refresh_token': azureRecords[x][1]
        }

        tokenData["access_token"] = fnCheckToken(
            tokenData["access_token"], tokenData["refresh_token"])

        chatResp = requests.get("https://graph.microsoft.com/v1.0/me/chats?$top=50",
                                headers={"Authorization": "Bearer " + tokenData["access_token"]})

        if chatResp.status_code == 200:
            chatsResponse = json.loads(chatResp.text)

            for item in chatsResponse['value']:
                # if item['chatType'] == "meeting":
                chatId = item['id']

                msgResp = requests.get(f"https://graph.microsoft.com/v1.0/chats/{chatId}/messages", headers={
                                       "Authorization": "Bearer {}".format(tokenData["access_token"])})

                if msgResp.status_code == 200:
                    msgResponse = json.loads(msgResp.text)

                    for msg in msgResponse['value']:
                        if msg['eventDetail'] != None and 'callRecordingUrl' in msg['eventDetail'] and msg['eventDetail']['callRecordingUrl'] != None and msg['eventDetail']['callRecordingStatus'] == "success":
                            logging.info("Topic : {}".format(item['topic']))
                            logging.info("Recording URL : {}".format(
                                msg['eventDetail']['callRecordingUrl']))

                            try:
                                upload_data = {
                                    'recording_url': msg['eventDetail']['callRecordingUrl'],
                                    'file_name': msg['eventDetail']['callRecordingDisplayName'],
                                    'access': tokenData["access_token"],
                                    'workspace': db
                                }
                                logging.info(json.dumps(upload_data))
                                # 1sec timeout
                                requests.post(
                                    url="https://nudgemeet-staging.azurewebsites.net/api/upload_files", json=upload_data, headers={"Content-Type": "application/json"}, timeout=1)
                                # requests.post(
                                #     url="http://localhost:7071/api/upload_files", json=upload_data, headers={"Content-Type": "application/json"}, timeout=1)
                            except:
                                pass

                            logging.info(
                                "--------------------------------------------------------------------------------------")
        else:
            logging.info("Authentication failed")

    return func.HttpResponse(
        "Success",
        status_code=200
    )
