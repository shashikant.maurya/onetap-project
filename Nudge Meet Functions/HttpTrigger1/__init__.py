import logging
import paramiko  

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('API called from trigger')

    userToCreate = 'testingUser'

    ip = '20.235.65.190'
    username = 'qliksenseserver\qsadmin'
    password = '123@Exponentia@123'
    cmd = 'net user ' + userToCreate + ' /add'
    
    try:
        print("Establishing connection to %s" %ip)
        # connect to server   
        con = paramiko.SSHClient()
        con.set_missing_host_key_policy(paramiko.AutoAddPolicy())   
        con.load_system_host_keys()   
        con.connect(ip, username=username, password=password)   
        print("Connection established")
        
        con.exec_command(cmd)

        print("Connection closing")
        con.close()
    except Exception as ex:
        print("Your Username and Password of are wrong.")

    return func.HttpResponse(
        "Success",
        status_code=200
    )
