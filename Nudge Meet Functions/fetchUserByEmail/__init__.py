import logging
import psycopg2
import pandas as pd
import json
import jwt
import os
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userEmail = req.params.get('email')

    if userEmail:
        authConn = createDBConnection('Authentication')
        authCursor = authConn.cursor()

        authCursor.execute(f"""   
                    SELECT 
                    emp_id, emp_name, email, 
                    password, workspaceid, is_otp, company_name,
                    role, phone, 
                    mobile_number, created_date,
                    emp_code,
                    database_name,
                    w.module_assigned
                    FROM central.employee as e
                    INNER JOIN central.workspace as w
                    ON e.workspaceid = w.name WHERE email = '{userEmail}'
                    """)
        columns = [column[0] for column in authCursor.description]
        pdf = pd.DataFrame(columns=columns)
        records = authCursor.fetchall()

        if(len(records) == 0):
            raise NameError(
                '{"errorMsg": "User doesn\'t exist."}', 404)
        else:
            for i in range(0, len(columns)):
                temp = []
                for j in records:
                    temp.append(j[i])
                pdf[str(columns[i])] = temp

        result = pdf.to_json(orient='records')
        parsed = json.loads(result)

        emp_id = parsed[0]['emp_id']
        role = parsed[0]['role']

        userConn = createDBConnection(parsed[0]["database_name"])
        userCursor = userConn.cursor()

        userCursor.execute(f"""
        SELECT 
        emp_id
        FROM nudge.employee
        WHERE email = '{userEmail}'
        """)

        emp_id_columns = [column[0] for column in userCursor.description]
        emp_id_pdf = pd.DataFrame(columns=emp_id_columns)

        emp_id_records = userCursor.fetchall()
        emp_id_parsed = []

        if(len(emp_id_records) != 0):
            for i in range(0, len(emp_id_columns)):
                temp = []
                for j in emp_id_records:
                    temp.append(j[i])
                pdf[str(emp_id_columns[i])] = temp

            emp_id_result = pdf.to_json(orient='records')
            emp_id_parsed = json.loads(emp_id_result)
            emp_id = emp_id_parsed[0]["emp_id"]

        subordinates = None
        if(len(emp_id_parsed)):
            subordinates_query = f'SELECT nudge.getSubordinates(\'{emp_id_parsed[0]["emp_id"]}\')'
            userCursor.execute(subordinates_query)
            subordinates_columns = [column[0]
                                    for column in userCursor.description]
            # subordinates_pdf = pd.DataFrame(columns=subordinates_columns)

            subordinates_records = userCursor.fetchall()

            if(len(subordinates_records) > 0):
                for i in range(0, len(subordinates_columns)):
                    temp = []
                    for j in subordinates_records:
                        if(j[i] != None):
                            temp.append(j[i])
                    subordinates = ','.join(temp)

            subordinates = subordinates

            token = jwt.encode(payload={
                "emp_id": emp_id,
                "name": parsed[0]['emp_name'],
                "mobile_number": parsed[0]['mobile_number'],
                "role": parsed[0]["role"],
                "tenantid": parsed[0]["workspaceid"],
                "database_name": parsed[0]["database_name"],
                "email": parsed[0]['email'],
                "company_name": parsed[0]['company_name'],
                "subordinates": None,
                "module_assigned": parsed[0]['module_assigned'],
            }, key='MfwtvVmtrmv!_v1')

            resp = {
                "message": "Employee is not present in ingestion table",
                "emp_id": emp_id,
                "root_workspace_id": parsed[0]["workspaceid"],
                "mobile_number": parsed[0]['mobile_number'],
                "name": parsed[0]['emp_name'],
                "role": parsed[0]['role'],
                "email": parsed[0]['email'],
                "token": token,
                "company_name": parsed[0]['company_name'],
                "subordinates": None,
                "module_assigned": parsed[0]['module_assigned'],
            }

            respJson = json.dumps(resp, indent=4, sort_keys=True)

        authCursor.close()
        authConn.close()

        userCursor.close()
        userConn.close()

        return func.HttpResponse(respJson)

        # return func.HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully.")
    else:
        return func.HttpResponse("Please provide valid data", status_code=500)
