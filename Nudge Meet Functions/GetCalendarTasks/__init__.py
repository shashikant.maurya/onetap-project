import logging
import psycopg2
import json
import pandas as pd
import requests
import jwt
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def refreshGoogleToken(refreshToken, userId, conn, cursor):
    # if Access token has expired fetch new access token
    clientId = '605586451222-tdl91au87jnjt3on5vujce7juakis4b4.apps.googleusercontent.com'
    data = {
        'client_id': clientId,
        'refresh_token': refreshToken,
        'grant_type': 'refresh_token',
        'enablePKCE': False
    }
    tokenResp = requests.post(
        "https://oauth2.googleapis.com/token", data)

    if(tokenResp.status_code == 200):
        jsonData = json.loads(tokenResp.text)
        accessToken = jsonData['access_token']

        cursor.execute("UPDATE google_oauth_tokens SET access_token = '" +
                       jsonData['access_token'] + "' WHERE user_id='" + userId + "';")
        conn.commit()
    return accessToken


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userId = req.params.get('id')
    today = req.params.get('date')

    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    conn = createDBConnection(dbName)
    cursor = conn.cursor()

    cursor.execute(
        "SELECT access_token, refresh_token FROM google_oauth_tokens WHERE user_id = '" + userId + "';")

    records = cursor.fetchone()
    if(cursor.rowcount > 0):
        accessToken = records[0]
        refreshToken = records[1]

        temp = requests.get(
            'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + accessToken)

        if(temp.status_code != 200):
            accessToken = refreshGoogleToken(
                refreshToken, userId, conn, cursor)

        resp = requests.get(
            "https://www.googleapis.com/calendar/v3/calendars/primary/events?timeMax={}T23:59:59Z&timeMin={}T00:00:00Z".format(today, today), headers={"Authorization": "Bearer " + accessToken})

        finalData = []

        if(resp.status_code == 200):
            calendarResp = json.loads(resp.text)
            for i in range(0, len(calendarResp["items"])):
                temp = {
                    'summary': calendarResp["items"][i]["summary"],
                    'start': calendarResp["items"][i]["start"]['dateTime']
                }
                finalData.append(temp)

        return func.HttpResponse(
            json.dumps(finalData),
            status_code=200
        )

    return func.HttpResponse(
        "User not logged in",
        status_code=500
    )
