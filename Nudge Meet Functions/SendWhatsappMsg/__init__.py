import logging
import jwt
import json
from datetime import datetime, timedelta
from cryptography.hazmat.primitives import serialization
import requests
import azure.functions as func


def getParameterByName(param, request):
    returnVal = None
    try:
        returnVal = request.params.get(param)
    except:
        print('Value not provided for ' + param)
    finally:
        return returnVal


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    intentName = getParameterByName('intent', req)
    apiMode = getParameterByName('env', req) or "prod"

    if(intentName == "" or intentName == None):
        return func.HttpResponse('Please provide a valid intent name', status_code=400)

    config = {
        "google_auth_url": "https://oauth2.googleapis.com/token"
    }

    if(apiMode.lower() == "dev"):
        config['bot_id'] = "mybotaman-vaae"
        config['session_id'] = "1e243d80-7fe7-fe7c-602e-01f1e32d34e2"
        config['project_version'] = "v2beta1"
        config['kid'] = "45d99335300422c04028731cca663237f8edbb58"
        config['service_account_name'] = "dialogflow-client-api-demo@mybotaman-vaae.iam.gserviceaccount.com"
        config['private_key'] = b"-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCZeJvmbUvY6jtd\n6PW/Rf62jYUGSQ7/xucX3Uqr85ZfwAm14OwDsmRBKsxUaSKBiTSCgxLgnJEV60vQ\nNXKg37X287dT6cv/G5TtaVNG34fgZvkWsDxJrJ8DlZdPrPTHMQrTwG1lB4xcy26M\n7fU/buNpUPnuj1VifOIB8mhJ0CiXTvFfXY5QEO0tb7mo1T9hBmP6u/96zvJwjNT3\nD4nGTlqENbXfZT//md1KuwGdb2bdS3wsjl6tqsArpIhTG8bqR3WK+02UgcVascSR\nNvOTjLo+3aF3hP3rc2bMPr+hEwaoxn6Ai4fufcfCMtKtkJrYYf4kiEtGpXA7p6fT\nNIY0P4iDAgMBAAECggEANKur9Z01FrDsxwIYtRh0nnpbWRjhyO6eiRYtdm4XAa2K\np50V4v7qYWMq1R1v9jz8hH4vkF6rfHZQgpiVSpj8flCOboIivN7eccxgBEba/yzX\n9+1sA2z29ef1Eictqt8t9BZqdO3uQjqfKkbQkICiE5by7+jJSQ/LERf8qriB9cte\nAn+izxy5jgWNirlOViaNnffaptsxGVQgOupa5Q1C0GNTN+QTI8VovnCXinYTJzll\nFLPJRaKxK4EZfyPTo6jxUo4Po45zx3dDwMGQDoBVqGfa86JKXXwIZMv859gXOhJC\nnL45Ke6lkhaH9kC/tNXhiZwXx+CzEEbYjTvGNyjUoQKBgQDI5EbSQvMKZUj0He2a\n1PRyZn8vxckDe9XPYxEX7/+DtfIBoHwcWY4iW7f6SJR/NT5aK5EMoZMVeGxanEpB\nLEL5PxPNSFKQcD2mnnQc9OT9cymzKI2LtgjWd/iSRr4HVDj5/dWdVlPzgPIhPESs\nPBEVKDahTX8QwvvF+V/5EoKkRQKBgQDDkjKsZ3K9vKSo23SsnJYcr/4RU56Km5qj\n+n3yL3LaSuTUZZ3wbqICjL+r1RZMtlczx/q/nYHN9W+S6yjnONfy2vVFXeqPIQ07\n81aFCLq4dlJrMhaQZVRHkS9wjZWcd0Xw4HzCBx2WgvKLgmgi/pdnpfsOIjXF8TS6\nVRGnu1uaJwKBgBUqgA+9QBAzVblqwdMTZuH6zKHC7Afy7zcXkAB+p+Q/skzblNgD\n04bpNPwo04SHnpcvievTxkR32Kq/Vs+BE1lhfPufCBvxx30bqs42Oz48+QLIXgl/\nV3UJVOZMj88D37DvPqeCJfw89MXtrKqc9YvOHdtm5+FjOdXFDYiXm64lAoGAUZbT\ndyNtNgTWOO/dHAnzgiJV/0qAUNkbyZ8+HeR3qOQn1QhTvLOa+S9Y40nxlK1ZcevT\nC0+dkJv9ITDkDpqjVNjODFNjALVMPR+Gj9eVdlE+jyQFT9ZGM9k5Dl6y9OS5lAVD\nZvrq+BjHMTaFVVwbNA2F0F6rSloXokjNJcm/LG8CgYA3E9OJXzhtXGKAnPDg5a2b\n1gTz9ZBynuGrByp03ZMbX2Lh8FguLp2IKoVeged6nbnM7lMDIEt4oZfiIpkgo34O\nxK2nwgpGnlUVGbOwIhwISk7mqyG+Kry32p1f/HMMeMwNJQ7iazA2aJwJdnOvLrDL\nZ2aBSfR5NbGo/fHw+TCG3w==\n-----END PRIVATE KEY-----\n"
    elif(apiMode.lower() == "prod"):
        config['bot_id'] = "newbot-xglp"
        config['session_id'] = "767234c5-255f-aeb3-7161-1fe49bfc2493"
        config['project_version'] = "v2"
        config['kid'] = "90fb11032f250afafd3a84f24f0f5c529aa25e1f"
        config['service_account_name'] = "newbotservice@newbot-xglp.iam.gserviceaccount.com"
        config['private_key'] = b"-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDfslrPS1JSnNMb\nCfXuFeHTjRFN/K7KL5wGVpKqUmQuVon0w/nSVrFk5gr1DUUqXuH4arArHcIsDlOz\nswTF35eQQ1d3s40zyHc53fi+OJ4+EtHn5gfeMHA6RvOVyJu3UDEeUOOTEMQZbNjA\nhnWQ9SE2dXrbhTPjaWHty2WPEMwtZ8qzfVr5k36wNuTsKDbC1j9BwlfA0zenLcOw\nmzg8ugXiEFD3lUJNlf2XzsTc9PFVY4ov8cvzpCokPhWs8tcbhKWNFquBweITwra8\nod/n+7uDoF4idQUEDMQSJPEOELojDsCOpW2gEjq3CoKRQTIBPoOhsZ8VoOQoJIQZ\nUs8ROIBdAgMBAAECggEAHs5/vhOX80OguRUOlurRjmJbkzts0PBsktziVu2A2XbW\niexhX8vLo2Mg+C2LLHIjuaPrvgxbnNieQ12wlxWbWGVFyvjhxdNiRAhjRMMkW9UY\nrlCaHcna8tOpoSOe+khjc1LdfHfvIbdBRIZf84YmvErCkC/QfQ2azHZZ9rFwBQKH\n16AWe460/RvzCCbIijP7Am0HLZvkRPnKZbFFMr7fojPxUXms3Aa+J5oslG/A7xJs\n3ruPZ1i5K5ZpKzB+TEcOyCQV0H2HuKdu3pC9WnbOO11GzM9JtE2xyPyElZVhWAmJ\n5RASe4d98SUi7BF/S0UUkVj+z+EEhINwrXUTtYGaQQKBgQDx8k0BjFjqFX6VzaN9\nURTdp+DTI1lfqY4K5wNw3YyyCX4DQPr4PDlLaMgKeWZUGHeZNQKeQ26NJAEMastV\nJEV5mq4bAxSgXU8/9jHoS/c7JBazPqia43sc3O+f8lCvjGfPsT5wx0u9hryb+hJy\nrnhkWZg4AyA9CUxReGPSOvhNoQKBgQDssK7V+8DBZX8b4Guox+WC1Q+/n2q6pt7k\naXK1xXv4aYYigMDwqJUQg0lg5adWUYvcEkeoBnYDcXOLbrlQR0e1I2lCt+fddGL/\nedKpj1WWu8NpghiOun1QR+8kYBgrrXIr4ztnrV1moqbggCrdiaXVlGH0Etynlm29\n8RVNhCRhPQKBgQDl6U996iVQBC7vIaN7YUJHw9DkMpIqUpA1EgN2DEI3qMeDRgt3\nOptjamW1dllkDoAQKYycYg26t3z7lLw14yrV/MP3zTybiy/U4xnX2eBM5eVgXWmR\nDhweeGCJCJXdy7mWZqKkPu5dYpz2R3OiYOLs0SrSBz5qYdyRCqsoynrMIQKBgF5J\nZ+xwNHIWN5NoTBrZPrFmpLZmEAM41FPTOdH1UJQ9nSSDe9iin3cLhaCm+4kkRP2u\n/oKNyFdIy4mA2yuCqasb5yOf1Srf4Uv6QJQxOBwqwgZ2v8ejvKXNchNggGBt4Kxh\nAdokMq/fHXETPYAxBDHXwUmUdfgAscjXj+/oMwHVAoGAQ3A8P92Y6HY+IduU4D03\nMOUEdo2RM8Aa6h8YadlqmQ9vU0pWvxGAvHfJ7ubAN2w+1jdhAojEWA+Hobo3VQQ2\n03bdqdfGkSr8UcZZheEfDKA/8qW+iHFDdqmHdcpu8IBnRCXCQyniAK2fKzYp0dpN\nuFFSn/nUKUUSSGo+3pdwerA=\n-----END PRIVATE KEY-----\n"
    else:
        return func.HttpResponse('Invalid data passed', status_code=412)

    config['dialogflow_url'] = "https://dialogflow.clients6.google.com/{}/projects/{}/agent/sessions/{}:detectIntent".format(
        config['project_version'], config['bot_id'], config['session_id'])

    header = {
        "alg": "RS256",
        "typ": "JWT",
        "kid": config['kid']
    }

    data = {
        "iss": config['service_account_name'],
        "scope": "https://www.googleapis.com/auth/dialogflow",
        "aud": "https://oauth2.googleapis.com/token",
        "iat": datetime.now().timestamp(),
        "exp": (datetime.now() + timedelta(hours=1)).timestamp()
    }

    loaded_private_key = serialization.load_pem_private_key(
        config['private_key'], password=None)

    # Encrypt the data using the public key.
    ciphertext = jwt.encode(
        data, loaded_private_key, algorithm="RS256", headers=header)

    authBody = {
        "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
        "assertion": ciphertext
    }

    response = requests.post(config['google_auth_url'], data=authBody)

    if(response.status_code == 200):
        responseBody = response.json()

        dialogBody = {
            "queryInput": {
                "text": {
                    # "text": "notification test",
                    "text": intentName,
                    "languageCode": "en"
                }
            },
            "queryParams": {
                "source": "DIALOGFLOW_CONSOLE",
                "timeZone": "Asia/Calcutta",
                "sentimentAnalysisRequestConfig": {
                    "analyzeQueryTextSentiment": "true"
                }
            }
        }

        dialogResponse = requests.post(config['dialogflow_url'], data=json.dumps(dialogBody), headers={
                                       'Authorization': 'Bearer ' + responseBody['access_token']})

        if dialogResponse.status_code != 200:
            return func.HttpResponse('Unable to send whatsapp message', status_code=500)
    else:
        return func.HttpResponse('Unable to fetch access token', status_code=500)

    return func.HttpResponse("Notification Sent Successfully", status_code=200)
