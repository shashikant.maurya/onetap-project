adal==1.2.6
adlfs==0.6.3
aiohttp==3.7.4.post0
appnope==0.1.3
async-timeout==3.0.1
attrs==20.3.0
autopep8==1.5.6
azure-common==1.1.27
azure-core==1.20.1
azure-datalake-store==0.0.52
azure-functions==1.8.0
azure-identity==1.6.1
azure-mgmt-core==1.3.0
azure-mgmt-media==3.0.0
azure-mgmt-resource==20.0.0
azure-storage-blob==12.6.0
backcall==0.2.0
bcrypt==4.0.1
Beaker==1.11.0
beautifulsoup4==4.9.3
cachetools==4.2.2
certifi==2020.12.5
cffi==1.14.5
chardet==4.0.0
click==8.0.1
colorama==0.4.4
cryptography==3.4.6
dateparser==1.0.0
DateTime==4.3
debugpy==1.4.3
decorator==4.4.2
deltalake==0.4.4
entrypoints==0.3
filelock==3.4.0
FormEncode==2.0.0
fsspec==0.8.7
future==0.18.2
google-api-core==1.31.1
google-api-python-client==2.15.0
google-auth==1.34.0
google-auth-httplib2==0.1.0
google-auth-oauthlib==0.4.5
googleapis-common-protos==1.53.0
GoogleNews==1.5.8
gssapi==1.8.2
hive==0.1.3.dev0
httplib2==0.19.1
idna==2.10
imageio==2.10.5
imageio-ffmpeg==0.4.5
ipykernel==6.4.1
ipython==7.27.0
ipython-genutils==0.2.0
isodate==0.6.0
jedi==0.18.0
Jinja2==2.11.3
joblib==1.0.1
jupyter-client==7.0.3
jupyter-core==4.8.1
krb5==0.4.1
Mako==1.1.4
MarkupSafe==1.1.1
matplotlib-inline==0.1.3
moviepy==1.0.3
msal==1.10.0
msal-extensions==0.3.0
msrest==0.6.21
msrestazure==0.6.4
multidict==5.1.0
Naked==0.1.31
nest-asyncio==1.5.1
nltk==3.6.2
nose==1.3.7
numpy==1.20.1
oauthlib==3.1.0
packaging==21.0
pandas==1.2.3
paramiko==3.0.0
parso==0.8.2
Paste==3.5.0
PasteDeploy==2.1.1
PasteScript==3.2.1
pickleshare==0.7.5
Pillow==8.4.0
portalocker==1.7.1
proglog==0.1.9
prompt-toolkit==3.0.20
protobuf==3.17.3
psycopg2-binary==2.8.6
pyarrow==3.0.0
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycodestyle==2.7.0
pycparser==2.20
pydub==0.25.1
Pygments==2.8.1
PyHive==0.6.3
PyJWT==2.1.0
Pylons==1.0.3
pyparsing==2.4.7
PySocks==1.7.1
python-dateutil==2.8.1
python-magic==0.4.25
pytz==2021.1
PyYAML==5.4.1
pyzmq==22.3.0
regex==2021.4.4
repoze.lru==0.7
requests==2.25.1
requests-oauthlib==1.3.0
Routes==2.5.1
rsa==4.7.2
shellescape==3.8.1
simplejson==3.17.2
six==1.15.0
soupsieve==2.2.1
Tempita==0.5.2
textblob==0.15.3
thrift==0.13.0
toml==0.10.2
tornado==6.1
tqdm==4.61.1
traitlets==5.1.0
twilio==7.3.0
typing-extensions==3.7.4.3
tzlocal==2.1
uritemplate==3.0.1
urllib3==1.26.4
uuid==1.30
waitress==2.0.0
wcwidth==0.2.5
WebError==0.13.1
WebHelpers==1.3
WebOb==1.8.7
WebTest==2.0.35
yarl==1.6.3
zope.interface==5.4.0
