import logging
import psycopg2
import json
import jwt
import azure.functions as func


def createDBConnection(dbName):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbName, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    isSuccess = False
    msg = ''
    try:
        userId = req.params.get('id')
        email = req.params.get('email')
        tokenType = req.params.get('type')

        decodedToken = verify_auth(str(req.headers.get('Authorization')))

        dbName = decodedToken['database_name']

        conn = createDBConnection(dbName)
        cursor = conn.cursor()

        if req.method.lower() == 'get':
            logging.info('Request for getting oauth token info')
            cursor.execute(
                f"SELECT access_token FROM oauth_tokens WHERE user_id='{userId}' AND token_type='{tokenType.lower()}';")
        elif req.method.lower() == 'post':
            logging.info('Request for saving oauth token details')
            req_body = req.get_json()
            accessToken = req_body.get('access')
            refreshToken = req_body.get('refresh')
            cursor.execute(
                f"""INSERT INTO oauth_tokens(user_id, email_id, token_type, access_token, refresh_token)
                SELECT '{userId}', '{email}', '{tokenType.lower()}', '{accessToken}', '{refreshToken}'
                WHERE NOT EXISTS (
                    SELECT 1 FROM oauth_tokens WHERE user_id = '{userId}' AND token_type = '{tokenType.lower()}'
                );""")
        elif req.method.lower() == 'put':
            logging.info('Updating the access token')
            req_body = req.get_json()
            accessToken = req_body.get('access')
            cursor.execute(
                f"""UPDATE oauth_tokens SET access_token='{accessToken}', modified_at=current_timestamp
                WHERE user_id='{userId}' AND token_type='{tokenType.lower()}';""")
        elif req.method.lower() == 'delete':
            logging.info('Delete the oauth token details')
            cursor.execute(
                f"DELETE FROM oauth_tokens WHERE user_id = '{userId}' AND token_type='{tokenType.lower()}';")

        if req.method.lower() != 'get':
            conn.commit()
            msg = 'Success'
        else:
            records = cursor.fetchall()
            data = {
                'accessToken': records[0][0]
            }
            msg = json.dumps(data)

        isSuccess = True

    except Exception as ex:
        logging.info("An exception of type {0} occurred. Arguments:\n{1!r}".format(
            type(ex).__name__, ex.args))

    finally:
        cursor.close()
        conn.close()

    return func.HttpResponse(
        msg if (isSuccess) else "Some error occurred",
        status_code=200 if (isSuccess) else 500
    )
