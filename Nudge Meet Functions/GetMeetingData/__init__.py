import logging
from datetime import datetime, timedelta
import jwt
import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import padding, rsa
from google.oauth2 import service_account
import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    header = {
        "alg": "RS256",
        "typ": "JWT",
        "kid": "ea7e5afae9efc0cb275c634e6b26838f18051088"
    }

    data = {
        "iss": "meetrecording@meetrecordingtest.iam.gserviceaccount.com",
        "sub": "shashikant.maurya@exponentia.ai",
        # "sub": "meetrecording@meetrecordingtest.iam.gserviceaccount.com",
        "scope": "https://www.googleapis.com/auth/drive.readonly",
        "aud": "https://oauth2.googleapis.com/token",
        "iat": datetime.now().timestamp(),
        "exp": (datetime.now() + timedelta(hours=1)).timestamp()
    }

    privateKey = b"-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC0HKMVMwXq19b5\nRYcy7tjgD/yzkhCxQBWk8ELHltNLzfFKOcb8gVU/jSUSiF8IafzRjZOylFl2Qgrj\nutQtgQ7EEPPiQYUdJEUymZJN+DLcZ+jYX3ouQL+iUzCKQDi85zJxXzJsOpfNcX8x\nYqvWie19CwrwNnYPG3IWiwjgVKklJ6pClM8uxhRs7pMOSV6ul1ip6U7/4yZSFrtU\nnoBfjtpFOmf5YpnNPaZbWzkiSDu2pFl+Wa1SZNjX2HhRW5WX8RSgkg1qzQfrMkCB\nK9ISPZ0uzomQWKwh3kOf4qbNoWChrYra2IJRHMKsHzBhwNhR+X7k2001EfINZvkv\nQFbjjv+DAgMBAAECggEABbjLLYc7buSAfdbJnsKbvTKE0qeXRCpHBcBkPrb563SV\nb8g6HBd3v9caEGaorjm/wGCBCrFPzkP22Jk82F07PNtVBCEUu4JFeIAMd9Gku//J\nE+akmQeRjvSZnCrCIq4tmhBuJ6YM4If0hHsf/mbPVqOdK1hvk7dJfpHYLFhEL1vv\nkt3kjjAxmNEV0Di0sDPUccXWFLSI1yIxu+l7AGa8UOE4TwqcFhuzEawZsu3UrvHe\nV3r0M3oNfnWwDSVGujvYWmPiDX700GoqSmMsRb7BUNBad3AyOVTZszfTRvTmTPh1\n18j7WoZDOK4AHibWhIv8azPmQYVPsw3Zp3Ka+Z8ncQKBgQDgGTmSc1e+xt/hb6fo\n/sQacR/T9mIt3M81mUWUallOrt2O0ulUCxFF/mbuz5ln1g1MWbgzd+RJfnPJ3B4I\nzzSn5sca05gvdYjStKiWpxG9VkphgvunQrSvnt7HuXFikUowuDwmf+4N+byIOS5I\nFur7KIwCiXaGTikubozOxvYAFQKBgQDNwGpPKn5YebVZLLUFb9Kr8yhsNyszhOQQ\nqX3xYe/cyZIQ8P8opucg/mDRNH4vBG0WUnVAAo42Q5t1TXwHT6ydJH3ryAUZdYqa\nPfJOPcvWD+NqOsbOFxl3eBj1m2uYhB8iCS4MM1nFy5CLnQUUHjjwh+PkcKPgwj9t\nJQDxlRTPNwKBgQDKZT3gDRh0jz1oZ+Wow1/t2bm7QE1PypvCdtmmv55FOB2WftqJ\nMy24n5nrmcedRm/gQy/gHCosTm7ZI1pk7m9KLV4p1m0ef/LBCrUwy1d0P1kSVLBg\nUhjKfJwMmmnTchEli1+YDfa83I7wIWSxazFzAR3itLA9gJ4k92do4b0QOQKBgQDD\n3YxWWKl2KEaO4ZRh3JW8wvmUKuChZryFcBRImWbx06GNJx5/4ILeUcpw3XvR3nDK\ncyzTBpvC5JE1FdIgU6SK45ZLwlm4ZsDPHHFyMjm9Sdq/4bwnrK29oUKBbLb8tam9\niOehDbFELgkoHw4anLqvn20+pkwbRk+C41sGn1jz+QKBgQC2AU967HeAKlEgtNTu\n6KipiZlyg6aPac5q7UU4ReVrq18itntM4N9GPTes742MMrrX8g+eWoGVDbbW4Vhu\nW5TYVkigLMAJj1ra7kOsjRRnE42kaWdFt7K1Ft12XjvahubFsi4X/NeIgOQeaAs2\nMzIUgw/6Z0I0SkITZ8hIOoN6IQ==\n-----END PRIVATE KEY-----\n"

    private_key = serialization.load_pem_private_key(
        privateKey, password=None)

    # Encrypt the data using the public key.
    ciphertext = jwt.encode(
        data, private_key, algorithm="RS256", headers=header)

    return func.HttpResponse(ciphertext, status_code=200)

    # name = req.params.get('name')
    # if not name:
    #     try:
    #         req_body = req.get_json()
    #     except ValueError:
    #         pass
    #     else:
    #         name = req_body.get('name')

    # if name:
    #     return func.HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully.")
    # else:
    #     return func.HttpResponse(
    #         "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
    #         status_code=200
    #     )
