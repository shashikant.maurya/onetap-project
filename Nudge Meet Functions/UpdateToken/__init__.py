import logging
import psycopg2
import json
import pandas as pd
import requests
import base64
import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def updateToken(conn, cursor, userId):

    cursor.execute(
        "SELECT email_id, access_token, refresh_token FROM google_oauth_tokens WHERE user_id = '" + userId + "';")

    if cursor.rowcount > 0:
        records = cursor.fetchall()
        userEmail = records[0][0]
        accessToken = records[0][1]
        refreshToken = records[0][2]
        resp = requests.get(
            "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + accessToken, headers={"Authorization": "Bearer " + accessToken})

        if(resp.status_code == 200):
            status = 200
        else:
            # if Access token has expired fetch new access token
            clientId = '605586451222-tdl91au87jnjt3on5vujce7juakis4b4.apps.googleusercontent.com'
            data = {
                'client_id': clientId,
                'refresh_token': refreshToken,
                'grant_type': 'refresh_token',
                'enablePKCE': False
            }
            tokenResp = requests.post(
                "https://oauth2.googleapis.com/token", data)

            if(tokenResp.status_code == 200):
                jsonData = json.loads(tokenResp.text)
                accessToken = jsonData['access_token']

                cursor.execute("UPDATE google_oauth_tokens SET access_token = '" +
                               jsonData['access_token'] + "' WHERE user_id='" + userId + "';")
                conn.commit()
                status = 200
            else:
                # Refresh token for the user has expired
                status = 204
    else:
        # User is not present in the database
        status = 204

    finalData = {
        'status': status,
        'email': userEmail,
        'accessToken': accessToken,
        'refreshToken': refreshToken
    }
    return finalData


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userId = req.params.get('id')

    # conn = createDBConnection('auth', '')
    # cursor = conn.cursor()

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE emp_id='{}';".format(userId))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchall()
    #     dbName = records[0][0]
    #     cursor.close()
    #     conn.close()

    dbName = 'expo_prod'

    conn = createDBConnection('', dbName)
    cursor = conn.cursor()

    tokenData = updateToken(conn, cursor, userId)

    # recipientemail = 'ved.verma@exponentia.ai'
    email = tokenData['email']
    currentPage = 1
    maxPage = 1

    for x in range(0, maxPage):
        leadsList = requests.get(
            'https://exponentia.freshsales.io/api/leads?page={}&per_page=100&segment_id=13000501247&sort=lead_score&sort_type=desc'.format(currentPage), headers={"Authorization": "Token token=BhgPF31Dc600VQGcKAjeyA"})
        leadsList = json.loads(leadsList.text)
        maxPage = leadsList['meta']['total_pages']
        for y in range(0, len(leadsList['leads'])):
            recipientemail = leadsList['leads'][y]['email']

            emailListResp = requests.get(
                'https://gmail.googleapis.com/gmail/v1/users/{}/messages?q={}'.format(email, recipientemail), headers={"Authorization": "Bearer " + tokenData['accessToken']})

            emailListResp = json.loads(emailListResp.text)

            for i in range(0, len(emailListResp['messages'])):
                emailResp = requests.get(
                    'https://gmail.googleapis.com/gmail/v1/users/{}/messages/{}'.format(email, emailListResp['messages'][i]['id']), headers={"Authorization": "Bearer " + tokenData['accessToken']})

                emailResp = json.loads(emailResp.text)

                snippet = emailResp['snippet']
                fromuser = ''
                messageDt = ''
                subject = ''
                receiver = ''
                cc = ''

                for j in range(0, len(emailResp['payload']['headers'])):
                    temp = emailResp['payload']['headers']
                    if(temp[j]['name'].lower() == "from"):
                        fromuser = temp[j]['value']
                    elif(temp[j]['name'].lower() == "date"):
                        messageDt = temp[j]['value']
                    elif(temp[j]['name'].lower() == "subject"):
                        subject = temp[j]['value']
                    elif(temp[j]['name'].lower() == "to"):
                        receiver = temp[j]['value']
                    elif(temp[j]['name'].lower() == "cc"):
                        cc = temp[j]['value']

                if(email in receiver):
                    messageType = 'Received'
                else:
                    messageType = 'Sent'

                cursor.execute("INSERT INTO gmail_info(lead_id, receiver_name, receiver_email, text, participants, attachments, message_type, created_date, leadowner_id, workspace_id, message_id) VALUES('13018841546', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(
                    receiver, email, subject, cc, '', messageType, messageDt, userId, "expo", emailListResp['messages'][i]['id']))
                conn.commit()

        currentPage = currentPage + 1

    # recipientResp = requests.get(
    #     'https://exponentia.freshsales.io/api/search?q={}&include={}'.format('ved', 'lead'), headers={"Authorization": "Token token=BhgPF31Dc600VQGcKAjeyA"})

    # recipientResp = json.loads(recipientResp.text)

    # recipientemail = recipientResp[0]['email']
    # useremail = recipientResp[0]['email']

    # emailListResp = requests.get(
    #     'https://gmail.googleapis.com/gmail/v1/users/{}/messages?q={}'.format(useremail, recipientemail), headers={"Authorization": "Bearer " + tokenData['accessToken']})

    # emailListResp = json.loads(emailListResp.text)

    # for i in range(0, len(emailListResp['messages'])):
    #     emailResp = requests.get(
    #         'https://gmail.googleapis.com/gmail/v1/users/{}/messages/{}'.format(useremail, emailListResp['messages'][i]['id']), headers={"Authorization": "Bearer " + tokenData['accessToken']})

    #     emailResp = json.loads(emailResp.text)

    #     snippet = emailResp['snippet']
    #     fromuser = ''
    #     messageDt = ''
    #     subject = ''
    #     receiver = ''
    #     cc = ''

    #     for j in range(0, len(emailResp['payload']['headers'])):
    #         temp = emailResp['payload']['headers']
    #         if(temp[j]['name'].lower() == "from"):
    #             fromuser = temp[j]['value']
    #         elif(temp[j]['name'].lower() == "date"):
    #             messageDt = temp[j]['value']
    #         elif(temp[j]['name'].lower() == "subject"):
    #             subject = temp[j]['value']
    #         elif(temp[j]['name'].lower() == "to"):
    #             receiver = temp[j]['value']
    #         elif(temp[j]['name'].lower() == "cc"):
    #             cc = temp[j]['value']

    #     if(useremail in receiver):
    #         messageType = 'Received'
    #     else:
    #         messageType = 'Sent'

    #     cursor.execute("INSERT INTO gmail_info(lead_id, receiver_name, receiver_email, text, participants, attachments, message_type, created_date, leadowner_id, workspace_id, message_id) VALUES('13018841546', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(
    #         receiver, useremail, subject, cc, '', messageType, messageDt, userId, "MOHL", emailListResp['messages'][i]['id']))
    #     conn.commit()

    cursor.close()
    conn.close()

    # if(tokenData.status == 204):
    #     return func.HttpResponse("User not found", status_code=204)
    # elif(tokenData.status == 200):
    return func.HttpResponse("Success", status_code=200)
    # else:
    #     return func.HttpResponse("User not found", status_code=401)
