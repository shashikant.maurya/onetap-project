import logging
import psycopg2
import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    req_body = req.get_json()
    accessToken = req_body.get('access')
    refreshToken = req_body.get('refresh')
    userId = req_body.get('userId')

    conn = createDBConnection('auth', '')
    cursor = conn.cursor()

    cursor.execute(
        "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE emp_id='{}';".format(userId))

    if cursor.rowcount > 0:
        records = cursor.fetchall()
        dbName = records[0][0]
        cursor.close()
        conn.close()

        conn = createDBConnection('', dbName)
        cursor = conn.cursor()

        cursor.execute(
            "SELECT access_token, refresh_token FROM google_oauth_tokens WHERE user_id = '" + userId + "';")

        if cursor.rowcount == 0:
            cursor.execute("INSERT INTO google_oauth_tokens(user_id, access_token, refresh_token) VALUES('" +
                           userId + "', '" + accessToken + "', '" + refreshToken + "');")
        else:
            cursor.execute(
                "UPDATE google_oauth_tokens SET access_token='" + accessToken + "', refresh_token='" + refreshToken + "' WHERE user_id='" + userId + "'")

        conn.commit()

        cursor.close()
        conn.close()

        return func.HttpResponse("Token is set", status_code=200)
    else:
        return func.HttpResponse("User not found", status_code=401)
