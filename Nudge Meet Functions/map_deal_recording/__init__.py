import logging
import psycopg2
import jwt
import requests
import json
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getValue(val):
    return '' if val == None else val


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    decodedToken = verify_auth(str(req.headers.get('Authorization')))
    dbName = decodedToken['database_name']

    req_body = req.get_json()
    recordingId = req_body.get('recordingId')
    dealId = req_body.get('dealId')
    dealStage = req_body.get('dealStage')
    accountName = req_body.get('accountName')
    recordingLink = req_body.get('recordingLink')
    callType = req_body.get('callType')

    conn = createDBConnection(dbName)
    cursor = conn.cursor()

    cursor.execute(
        f"UPDATE recording_master SET deal_id = '{dealId}', deal_stage = '{dealStage}', account_name = '{accountName}' WHERE recording_id = {recordingId};")

    conn.commit()

    requests.post(
        url="https://media-converter.azurewebsites.net/api/convert_in_person", data=json.dumps({
            "id": recordingId,
            "link": recordingLink,
            "db": dbName,
            "type": callType
        }), timeout=5)

    cursor.close()
    conn.close()

    return func.HttpResponse(
        "Success",
        status_code=200
    )
