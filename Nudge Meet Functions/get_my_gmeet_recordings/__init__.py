import logging
import psycopg2
import json
import pandas as pd
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userEmail = req.params.get('email')

    conn = createDBConnection('Authentication')
    cursor = conn.cursor()

    cursor.execute(f"""
        SELECT 
            database_name 
        FROM 
            central.employee e 
                JOIN 
            central.workspace w ON e.workspaceid = w.name
        WHERE e.email = '{userEmail}';
    """)

    if cursor.rowcount > 0:
        authRecords = cursor.fetchone()
        cursor.close()
        conn.close()

        dbName = authRecords[0]

        conn = createDBConnection(dbName)
        cursor = conn.cursor()

        cursor.execute(f"""
            SELECT 
                mr.*,
                e.*,
                edt.deal_name,
                edt.sales_account
            FROM 
                meet_recordings mr 
                    JOIN 
                employee e ON e.emp_id = mr.created_by AND mr.deal_id != '' AND mr.deal_id != 'None'
                    JOIN
                expo_deals_table edt ON CAST(mr.deal_id as BIGINT) = edt.id
            WHERE 
                e.email = '{userEmail}'
            ORDER BY recording_id DESC;
        """)

        if cursor.rowcount > 0:

            columns = [column[0] for column in cursor.description]
            pdf = pd.DataFrame(columns=columns)
            print(columns)

            r = [dict((cursor.description[i][0], value)
                      for i, value in enumerate(row)) for row in cursor.fetchall()]

            final = json.dumps((r[0] if r else None) if False else r)
            cursor.close()
            conn.close()

            return func.HttpResponse(
                final,
                status_code=200
            )
        else:
            return func.HttpResponse(
                "No content found",
                status_code=204
            )
    else:
        return func.HttpResponse(
            "User not authorized",
            status_code=401
        )
