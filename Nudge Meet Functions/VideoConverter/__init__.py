import logging
from moviepy.editor import *
import tempfile
from azure.storage.blob import BlobServiceClient
from pydub import AudioSegment
import azure.functions as func
import requests
import io
from os import listdir


def mp4_to_wav_mem(file):
    audio = AudioSegment.from_file_using_temporary_files(file, 'mp4')
    file = io.BytesIO()
    file = audio.export(file, format="wav", codec='pcm_s16le', bitrate=44100)
    file.seek(0)
    return file


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    # tempDirectory = tempfile.gettempdir()
    # fp = tempfile.NamedTemporaryFile()
    # # fp.write(b'Hello world!')
    # filesDirListInTemp = listdir(tempDirectory)
    upload_file_path = '1bASa4bbqWVLklVkM7Vo7LgWvSTmAi9V2.mp4'
    filePath = 'temp/1bASa4bbqWVLklVkM7Vo7LgWvSTmAi9V2.mp4'
    connect_str = 'DefaultEndpointsProtocol=https;AccountName=speecht;AccountKey=HvHhw2jAOj01vrsjTagWI33d+k7iQs4P1Q+CEGCERLvVMx6bUUgWnUub/2yqKBfq6LSXF6VHp7xusIHGUlaDiA==;EndpointSuffix=core.windows.net'
    container_name = 'container2'

    print('API is starting to process data')

    blob_client_instance = BlobServiceClient.from_connection_string(connect_str).get_blob_client(
        container=container_name, blob=filePath)

    # logging.info('Created a blob client')

    # blob_service_client_instance = BlobServiceClient(
    #     account_url=STORAGEACCOUNTURL, credential=STORAGEACCOUNTKEY)

    # blob_client_instance = blob_service_client_instance.get_blob_client(
    #     CONTAINERNAME, BLOBNAME, snapshot=None)

    # blob_data = blob_client_instance.download_blob()
    with open(upload_file_path, "wb") as download_file:
        download_file.write(blob_client_instance.download_blob().readall())
    # data = blob_data.readall()
    # print(data)

    # r = requests.get(
    #     'https://speecht.blob.core.windows.net/container2/temp/1bASa4bbqWVLklVkM7Vo7LgWvSTmAi9V2.mp4', stream=True)
    # file = io.BytesIO(r.content)
    # file = mp4_to_wav_mem(file)
    # VideoFileClip()

    # with open(upload_file_path, "wb") as my_blob:
    # download_stream = blob_client.download_blob()
    # fp.write(download_stream.readall())

    logging.info('Downloaded the video file for conversion')

    # convertedFile = mp4_to_wav_mem(fp.file)

    # clip = VideoFileClip(filePath)
    # clip.audio.write_audiofile(
    #     filePath)
    # audio = AudioSegment.from_file(filePath, format="mp4")
    # audio.export(filePath, format="wav")

    logging.info('Conversion to audio file done. Starting upload')

    # blob_client = BlobServiceClient.from_connection_string(connect_str).get_blob_client(
    #     container=container_name, blob=upload_file_path)

    # with open(filePath, "rb") as data:
    # blob_client.upload_blob(convertedFile)

    logging.info('Uploading to blob completed')

    return func.HttpResponse(
        "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
        status_code=200
    )
