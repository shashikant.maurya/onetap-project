import logging
import requests
import psycopg2
import json
from datetime import datetime, timedelta
import azure.functions as func


def getParameters(params, key):
    try:
        returnVal = params.get(key)
    except:
        returnVal = ""

    return returnVal


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getTokensFromDB(conn, userId):
    cursor = conn.cursor()

    cursor.execute(
        "SELECT access_token, refresh_token FROM google_oauth_tokens WHERE user_id = '" + userId + "';")

    val = {}

    if cursor.rowcount > 0:
        records = cursor.fetchall()
        val["accessToken"] = records[0][0]
        val["refreshToken"] = records[0][1]

    return val


def getDate(val):
    returnVal = {}

    if val.lower() == "today":
        start = datetime.now()
        end = datetime.today() + timedelta(days=1)
    elif val.lower() == "tommorrow":
        start = datetime.now() + timedelta(days=1)
        end = datetime.today() + timedelta(days=2)
    elif val.lower() == "yesterday":
        start = datetime.now() + timedelta(days=-1)
        end = datetime.today()
    else:
        dateStr = val.split("-")
        start = datetime(int(dateStr[0]), int(
            dateStr[1]), int(dateStr[2]), 0, 0, 0, 0)
        end = start + timedelta(days=1)

    returnVal["start"] = datetime(
        start.year, start.month, start.day, 0, 0, 0, 0).isoformat('T')
    returnVal["end"] = datetime(
        end.year, end.month, end.day, 0, 0, 0, 0).isoformat('T')

    return returnVal


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    status = 500

    dateQuery = getParameters(req.params, 'date')
    searchQuery = getParameters(req.params, 'search')
    userId = getParameters(req.params, 'id')

    logging.info('Getting data for user {}'.format(userId))
    logging.info('With date search for {}'.format(dateQuery))
    logging.info('With search query for {}'.format(searchQuery))

    searchDate = {}
    queryString = ""

    if(dateQuery != None and dateQuery != ""):
        searchDate = getDate(dateQuery)

    if (('start' in searchDate) or ('end' in searchDate)):
        queryString = "?timeMax=" + searchDate["end"] + \
            "Z&timeMin=" + searchDate["start"] + "Z"

    if(searchQuery != None and searchQuery != ""):
        queryString = queryString + \
            ("?" if queryString == '' else '&') + "q=" + str(searchQuery)

    logging.info(
        'Creating database connection for fetching data for user {}'.format(userId))

    conn = createDBConnection('auth', '')
    cursor = conn.cursor()

    cursor.execute(
        "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE emp_id='{}';".format(userId))

    if cursor.rowcount > 0:
        records = cursor.fetchall()
        dbName = records[0][0]
        cursor.close()
        conn.close()

        conn = createDBConnection('', dbName)

        logging.info('Database Connection created successfully')
        logging.info('Get token details for user {}'.format(userId))
        tokenData = getTokensFromDB(conn, userId)
        logging.info('Fetched data count is {} for user {}'.format(
            len(tokenData), userId))

        if (('accessToken' in tokenData) or ('refreshToken' in tokenData)):
            print(tokenData["accessToken"])
            # Fetch meetings data from api
            meetingResponse = requests.get(
                "https://www.googleapis.com/calendar/v3/calendars/primary/events" + queryString, headers={"Authorization": "Bearer " + tokenData["accessToken"]})
            if meetingResponse.status_code != 200:
                # if token is invalid update access token and re-fetch from database
                resp = requests.get(
                    "https://nudgemeet.azurewebsites.net/api/checkgoogletokens?id=" + userId)
                if resp.status_code != 200:
                    status = 500
                else:
                    tokenData = getTokensFromDB(conn, userId)

            else:
                meetingJson = json.loads(meetingResponse.text)
                for item in meetingJson['items']:
                    if 'attachments' in item:
                        for video in item['attachments']:
                            template = "https://drive.google.com/file/d/" + \
                                video['fileId'] + "/view"
                            # template = "https://drive.google.com/uc?export=download&id=" + \
                            #     video['fileId']
                            video["videoUrl"] = template

                status = 200
        else:
            logging.info('No tokens found for user {}'.format(userId))

        if status == 500:
            return func.HttpResponse(
                "Some error occurred",
                status_code=500
            )
        else:
            return func.HttpResponse(
                json.dumps(meetingJson['items']),
                status_code=200
            )
    else:
        return func.HttpResponse("User not found", status_code=401)
