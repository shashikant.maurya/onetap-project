import base64
import logging
import uuid

import azure.functions as func
import requests

from azure.storage.blob import BlobServiceClient, BlobBlock, ContentSettings

storage_account_name = "nudgeazurefuncstorage"
connection_string = "DefaultEndpointsProtocol=https;AccountName=nudgeazurefuncstorage;AccountKey=//m35hRuUsQwpAFtMcQ5SKj6dwsBgfpXMkXWX2S7twX7q6fpBCwuYbC3HE5KWl6xZucrf6eigMbNug4okDu0sA==;EndpointSuffix=core.windows.net"
container_name = "audio-files"


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    # for below steps refer : https://learn.microsoft.com/en-us/answers/questions/1000346/need-to-fetch-the-download-the-meeting-recording-f
    logging.info(req.get_json())
    req_body = req.get_json()
    callRecordingUrl = req_body.get('recording_url')
    fileName = req_body.get('file_name')
    access_token = req_body.get('access')
    workspace = req_body.get('workspace')

    encodedUrl = b'u!' + \
        base64.b64encode(callRecordingUrl.encode("ascii")).replace(
            b'/', b'_').replace(b'+', b'-')

    videoResp = requests.get("https://graph.microsoft.com/v1.0/shares/{}/driveItem/content".format(
        encodedUrl.decode("ascii")), headers={"Authorization": "Bearer {}".format(access_token)}, stream=True)

    filepath = f"{workspace}/original/azure_meetings/{fileName}"
    blob_service_client = BlobServiceClient.from_connection_string(
        connection_string)
    blob_client = blob_service_client.get_blob_client(
        container=container_name, blob=filepath)

    # upload 4 MB for each request
    CHUNK_SIZE = 4*1024*1024
    block_list = []

    logging.info(f'Uploading file : {fileName}')

    try:
        if videoResp.status_code == 200:
            for chunk in videoResp.iter_content(chunk_size=CHUNK_SIZE):
                if chunk:
                    block_id = str(uuid.uuid4())
                    blob_client.stage_block(block_id=block_id, data=chunk)
                    block_list.append(BlobBlock(block_id=block_id))

            blob_client.commit_block_list(block_list)
            blob_client.set_http_headers(
                ContentSettings(content_type='video/mp4'))
    except Exception as ex:
        logging.info("An exception of type {0} occurred. Arguments:\n{1!r}".format(
            type(ex).__name__, ex.args))

    logging.info(f'Uploaded file : {fileName}')

    return func.HttpResponse(
        "Success",
        status_code=200
    )
