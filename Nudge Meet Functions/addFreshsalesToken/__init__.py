import logging
import psycopg2
import requests
import jwt
import os
import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    req_body = req.get_json()
    domain = req_body.get('domain')
    email = req_body.get('email')
    token = req_body.get('token')
    userId = req_body.get('userId')
    workspaceId = req_body.get('workspace')

    # conn = createDBConnection('auth', '')
    # cursor = conn.cursor()

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE email='{}';".format(email))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     dbName = records[0]
    # cursor.close()
    # conn.close()

    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    conn = createDBConnection('', dbName)
    cursor = conn.cursor()

    cursor.execute(
        "SELECT e.email FROM employee e WHERE e.emp_id = '{}';".format(
            userId))

    records = cursor.fetchall()

    logging.info('Fetching data from freshsales')
    resp = requests.get(
        "{}api/search?q={}&include=user".format(domain, records[0][0]), headers={"Authorization": "Token token=" + token})

    logging.info('Status from freshsales = ' + str(resp.status_code))
    freshId = ''
    if resp.status_code == 200:
        print(resp)
        if(len(resp.json())):
            freshId = resp.json()[0]["id"]

    cursor.execute(
        "SELECT domain_url, token, freshsales_id, e.email FROM freshsales_credentials fc INNER JOIN employee e ON fc.leadownerid = e.emp_id WHERE fc.leadownerid = '{}';".format(
            userId))

    if cursor.rowcount == 0:
        logging.info('Inserting data into database')
        cursor.execute(
            "INSERT INTO freshsales_credentials(domain_url, token, leadownerid, freshsales_id) VALUES('" + domain + "', '" + token + "', '" + userId + "', '" + freshId + "');")
    else:
        logging.info('Updating data into database')
        cursor.execute(
            "UPDATE freshsales_credentials SET domain_url='{}', token='{}', freshsales_id = '{}' WHERE leadownerid='{}'".format(domain, token, freshId, userId))

    conn.commit()
    logging.info('Data added to database')

    cursor.close()
    conn.close()

    return func.HttpResponse("Token is set", status_code=200)
    # else:
    #     return func.HttpResponse("User not found", status_code=401)
