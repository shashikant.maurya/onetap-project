import logging
import psycopg2
import json
import jwt
import requests
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userId = req.params.get('id')
    req_body = req.get_json()
    title = req_body.get('title')
    description = req_body.get('description')
    location = req_body.get('location')
    startTime = req_body.get('startTime')
    endTime = req_body.get('endTime')
    attendees = req_body.get('attendees')

    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    # attendeesList = []

    # temp = requests.get(
    #     'https://nudgemeet.azurewebsites.net/api/UpdateToken?id=' + userId)

    # conn = createDBConnection('auth', '')
    # cursor = conn.cursor()

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE emp_id='{}';".format(userId))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchall()
    #     dbName = records[0][0]
    #     cursor.close()
    #     conn.close()

    dbName = decodedToken['database_name']

    conn = createDBConnection(dbName)
    cursor = conn.cursor()

    cursor.execute(
        "SELECT access_token, refresh_token FROM google_oauth_tokens WHERE user_id = '" + userId + "';")

    records = cursor.fetchone()
    accessToken = records[0]

    reqData = {
        "summary": title,
        "location": location,
        "description": description,
        "start": {
            "dateTime": startTime,
            # "dateTime": "2021-10-20T16:00:00+05:30",
            "timeZone": "Asia/Kolkata"
        },
        "end": {
            "dateTime": endTime,
            # "dateTime": "2021-10-20T16:05:00+05:30",
            "timeZone": "Asia/Kolkata"
        },
        "attendees": attendees
        # [
        #     {
        #         "email": "shashikant00786@gmail.com"
        #     }
        # ]
    }

    response = requests.post(
        'https://www.googleapis.com/calendar/v3/calendars/primary/events', data=json.dumps(reqData), headers={'Authorization': 'Bearer ' + accessToken})

    if(response.status_code == 200):
        return func.HttpResponse(
            "Success.",
            status_code=200
        )
    else:
        return func.HttpResponse(
            "Some error occurred.",
            status_code=500
        )
