import logging

import azure.functions as func
from twilio.rest import Client


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    req_body = req.get_json()
    sendMsgTo = req_body.get('to')
    msgType = req_body.get('type')

    account_sid = 'AC0b225129b1554a44382b4011f97ae3e8'
    auth_token = 'cffeb3b13ea13eb18ce9e93ade1f12b9'
    client = Client(account_sid, auth_token)

    if msgType == 'birthday':
        messageBody = 'Dear Customer, Onetap Wishes you a Very Happy Birthday and a Great Year ahead!'

    elif msgType == 'product_overview':
        messageBody = 'OneTap Mobile App overview:\nhttps://nudgeazurefuncstorage.blob.core.windows.net/audio-files/OneTap_Assets/OneTap_VoiceOver_Demo.mp4'

    message = client.messages.create(
        body=messageBody,
        from_='whatsapp:+13614203347',
        # media_url='https://nudgeazurefuncstorage.blob.core.windows.net/audio-files/OneTap_Assets/bokeh_blur_sun_nature_lights_park_471.mp4',
        to='whatsapp:{}'.format(sendMsgTo)
    )

    print(message.sid)

    return func.HttpResponse(
        "Success",
        status_code=200
    )
