import logging
import psycopg2
import json
import jwt
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getValue(val):
    return '' if val == None else val


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    result = []
    dbName = ""
    id = req.params.get('id')
    email = req.params.get('email')

    # conn = createDBConnection('Authentication')
    # cursor = conn.cursor()

    # cursor.execute(f"""
    #     SELECT
    #         database_name
    #     FROM
    #         central.employee e
    #             JOIN
    #         central.workspace w ON e.workspaceid = w.name
    #     WHERE e.email = '{email}';
    # """)

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     dbName = records[0]
    # cursor.close()
    # conn.close()
    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    conn = createDBConnection(dbName)
    cursor = conn.cursor()

    cursor.execute("""
        SELECT
            dealid, primary_id, (SELECT string_agg(trim(value::text, '"'), ', ')
                        FROM json_array_elements(t.actions)) AS list, edt.contact_name, t.date_conversation
        FROM
            sentiment_output_table t
                JOIN
            expo_deals_table edt ON t.dealid::bigint = edt.id
        WHERE
            primary_id = {} and action_taken = 0;
    """.format(id))

    if cursor.rowcount > 0:
        records = cursor.fetchall()
        for row in records:
            temp = {}
            temp["dealId"] = row[0]
            temp["primaryId"] = row[1]
            temp["actions"] = row[2]
            temp["contact_name"] = row[3]
            temp["date_conversation"] = row[4]
            result.append(temp)

    cursor.close()
    conn.close()

    return func.HttpResponse(
        json.dumps(result),
        status_code=200
    )
