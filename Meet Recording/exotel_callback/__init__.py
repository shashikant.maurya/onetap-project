import logging
import requests
from azure.storage.blob import BlobServiceClient, __version__, BlobBlock, BlobProperties, ContentSettings
import os
import magic
import psycopg2
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def moveFileToBlob(fileUrl):

    mime = magic.Magic(mime=True)
    connect_str = 'DefaultEndpointsProtocol=https;AccountName=nudgeazurefuncstorage;AccountKey=//m35hRuUsQwpAFtMcQ5SKj6dwsBgfpXMkXWX2S7twX7q6fpBCwuYbC3HE5KWl6xZucrf6eigMbNug4okDu0sA==;EndpointSuffix=core.windows.net'
    container_name = 'audio-files'

    upload_file_path = f'temp/Call-Recording/{os.path.basename(fileUrl)}'

    # Create a blob client using the local file name as the name for the blob
    blob_client = BlobServiceClient.from_connection_string(connect_str).get_blob_client(
        container=container_name, blob=upload_file_path)
    blob_client.upload_blob_from_url(fileUrl)
    blob_client.set_http_headers(ContentSettings(
        content_type=mime.from_file(os.path.basename(fileUrl))))


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    req_body = req.get_json()
    callSid = req_body.get('email')

    # cursor.execute("INSERT INTO recording_master(meeting_id, recording_type, name, phone_number, meet_title, meet_description, meet_link, duration, recording_url, participants, notes, summary, blob_path, original_blob_path, start_time, end_time, deal_id, deal_stage, account_name, created_by, created)
    #                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

    # downloadFile(
    #     fileUrl="https://s3-ap-southeast-1.amazonaws.com/exotelrecordings/exponentia2/3470166902ce90843b852b4145e2162p.mp3")

    return func.HttpResponse(
        "success",
        status_code=200
    )
