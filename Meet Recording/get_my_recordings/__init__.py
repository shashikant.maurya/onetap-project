import logging
import psycopg2
import json
import pandas as pd
import jwt
from psycopg2.extras import RealDictCursor
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    logging.info(
        f"Authorization Token received = {str(req.headers.get('Authorization'))}")
    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    logging.info("==== Decoded Token Value ====")
    logging.info(decodedToken)
    dbName = decodedToken['database_name']
    userId = decodedToken['emp_id']
    logging.info(f"Database name = {dbName}")
    logging.info(f"User Id = {userId}")

    conn = createDBConnection(dbName)
    logging.info("Database connection created")
    cursor = conn.cursor()
    logging.info("Cursor created")

    cursor.execute(f"""
        SELECT
            edt.deal_name,
            rm.* 
        FROM 
            recording_master rm
                LEFT JOIN
            expo_deals_table edt ON rm.deal_id = CAST(edt.id as character varying)
        WHERE
            created_by = '{userId}'
        ORDER BY recording_id DESC;
    """)
    logging.info(f"Query Execution successfull {cursor.rowcount}")
    if cursor.rowcount > 0:
        logging.info(f"Fetching all rows")
        columns = [column[0] for column in cursor.description]
        pdf = pd.DataFrame(columns=columns)
        logging.info(columns)

        records = cursor.fetchall()

        for i in range(0, len(columns)):
            temp = []
            for j in records:
                temp.append(j[i])
            pdf[str(columns[i])] = temp

        print(pdf.head())
        result = pdf.to_json(orient='records')
        parsed = json.loads(result)
        final = json.dumps(parsed, indent=4, sort_keys=True)

        cursor.close()
        conn.close()

        return func.HttpResponse(
            final,
            status_code=200
        )
    else:
        return func.HttpResponse(
            "No content found",
            status_code=204
        )
