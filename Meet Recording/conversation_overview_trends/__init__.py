import logging
import psycopg2
import json
import jwt
import os
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getValue(val):
    return '' if val == None else val


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    result = []
    dbName = ""
    # id = req.params.get('id')
    email = req.params.get('email')

    # conn = createDBConnection('Authentication')
    # cursor = conn.cursor()

    # cursor.execute(f"""
    #     SELECT
    #         database_name
    #     FROM
    #         central.employee e
    #             JOIN
    #         central.workspace w ON e.workspaceid = w.name
    #     WHERE e.email = '{email}';
    # """)

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     dbName = records[0]
    # cursor.close()
    # conn.close()

    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    conn = createDBConnection(dbName)
    cursor = conn.cursor()

    # cursor.execute("""
    #     SELECT
    #         AVG(sentiment) sentiment, AVG(monologue) monologue, AVG(coverage_score) coverage_score, date_conversation
    #     FROM
    #         sentiment_output_table sot
    #             JOIN
    #         employee e ON e.emp_id = sot.leadownerid
    #     WHERE
    #         e.email = '{}'
    #     GROUP BY dealid, date_conversation;
    # """.format(email))

    # cursor.execute("""
    # select ROUND(AVG(coverage_score))::int coverage_score, ROUND(AVG(monologue))::int monologue, ROUND(AVG(sentiment))::int sentiment, TO_DATE(updated_date,'DD-MM-YYYY')::character varying created_on
    # from sentiment_output_table sot JOIN employee e ON e.emp_id = sot.leadownerid
    # where TO_DATE(updated_date,'DD-MM-YYYY') > current_timestamp - interval '30 day' and e.email = '{}'
    # group by TO_DATE(updated_date,'DD-MM-YYYY')::date;
    # """.format(email))

    cursor.execute("""
    SELECT
        coalesce(coverage_score, 0) coverage_score, 
        coalesce(monologue, 0) monologue, 
        coalesce(sentiment, 0) sentiment, 
        dates::character varying created_on
    FROM
        (SELECT 
            ROUND(AVG(coverage_score))::int coverage_score,
            ROUND(AVG(monologue))::int monologue, 
            ROUND(AVG(sentiment))::int sentiment, 
            TO_DATE(updated_date,'DD-MM-YYYY')::character varying created_on
        FROM 
            sentiment_output_table sot 
                JOIN 
            employee e ON e.emp_id = sot.leadownerid
        WHERE 
            TO_DATE(updated_date,'DD-MM-YYYY') > current_timestamp - interval '30 day' and e.email = '{}'
        GROUP BY TO_DATE(updated_date,'DD-MM-YYYY')::date) x
            RIGHT JOIN
        (SELECT generate_series(current_timestamp - interval '30 day', current_timestamp, interval  '1 day')::date dates) y ON y.dates = x.created_on::date;
    """.format(email))

    if cursor.rowcount > 0:
        records = cursor.fetchall()
        for row in records:
            temp = {}
            temp["avg_coverage"] = row[0]
            temp["avg_monologue"] = row[1]
            temp["avg_sentiment"] = row[2]
            temp["created_on"] = row[3]
            result.append(temp)

    cursor.close()
    conn.close()

    return func.HttpResponse(
        json.dumps(result),
        status_code=200
    )
