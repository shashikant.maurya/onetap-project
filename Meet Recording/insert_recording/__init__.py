import json
import logging
import psycopg2
from twilio.rest import Client
import azure.functions as func
import requests
import jwt
import os
from azure.storage.blob import BlobServiceClient, ContentSettings
# from azure.storage.blob import BlobServiceClient, __version__, BlobBlock, BlobProperties, ContentSettings


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getValue(val):
    return '' if val == None else val


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    # dbName = "realestate_prod"

    userId = req.params.get('id')
    workspace = req.params.get('workspace')

    req_body = req.get_json()
    email = req_body.get('email')
    name = getValue(req_body.get('name'))
    phone_number = getValue(req_body.get('phone_number'))
    call_type = getValue(req_body.get('call_type'))
    start_time = getValue(req_body.get('start_time'))
    end_time = getValue(req_body.get('end_time'))
    recording_link = getValue(req_body.get('recording_link'))
    notes = getValue(req_body.get('notes'))
    deal_id = getValue(req_body.get('deal_id'))
    account = getValue(req_body.get('account'))
    stage = getValue(req_body.get('stage'))
    dealName = getValue(req_body.get('deal'))

    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    # conn = createDBConnection('auth', '')
    # cursor = conn.cursor()

    # cursor.execute(f"""
    #     SELECT
    #         database_name
    #     FROM
    #         central.employee e
    #             JOIN
    #         central.workspace w ON e.workspaceid = w.name
    #     WHERE e.email = '{email}';
    # """)

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE email='{}';".format(email))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     dbName = records[0]
    # cursor.close()
    # conn.close()

    conn = createDBConnection('', dbName)
    cursor = conn.cursor()

    name = recording_link.split('/')[-1].replace("%20", " ")

    # cursor.execute("INSERT INTO call_recording(name, phone_number, call_type, recording_link, notes, deal_id, start_time, end_time, leadowner_id, workspaceid, summary, recording_type, stage, account_name, deal_name) VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}');".format(
    #     name, phone_number, call_type, recording_link, notes, deal_id, start_time, end_time, userId, workspace, '', '', stage, account, dealName))

    cursor.execute("INSERT INTO recording_master(recording_type, name, phone_number, original_blob_path, notes, deal_id, start_time, end_time, created_by, deal_stage, account_name) VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}');".format(
        call_type, name, phone_number, recording_link, notes, deal_id, start_time, end_time, userId, stage, account))

    conn.commit()

    cursor.execute(
        "SELECT recording_id FROM recording_master WHERE original_blob_path='{}'".format(recording_link))
    if cursor.rowcount > 0:
        records = cursor.fetchone()
        recordingId = records[0]

    requests.post(
        url="https://media-converter.azurewebsites.net/api/convert_in_person", data=json.dumps({
            "id": recordingId,
            "link": recording_link,
            "db": dbName,
            "type": call_type
        }), timeout=5)

    # cursor.execute(
    #     "SELECT email FROM employee WHERE emp_id = '{}';".format(userId))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     emailId = records[0]

    #     conn1 = createDBConnection('auth', '')
    #     cursor1 = conn1.cursor()

    #     cursor1.execute(
    #         "SELECT mobile_number FROM central.employee WHERE email = '{}';".format(emailId))

    #     if cursor1.rowcount > 0:
    #         records1 = cursor1.fetchone()
    #         mobileNumber = records1[0]
    mobileNumber = decodedToken['mobile_number']

    if call_type.lower() != 'meeting':
        # Find your Account SID and Auth Token at twilio.com/console
        # and set the environment variables. See http://twil.io/secure
        # account_sid = os.environ['TWILIO_ACCOUNT_SID']
        account_sid = 'AC0b225129b1554a44382b4011f97ae3e8'
        # auth_token = os.environ['TWILIO_AUTH_TOKEN']
        auth_token = 'cffeb3b13ea13eb18ce9e93ade1f12b9'
        client = Client(account_sid, auth_token)

        message = client.messages.create(
            body='A new call recording is available: \n Name: {} \n Deal ID: {} \n Phone Number: {} \n Call Type: {} \n For more information visit the app: https://exponentia.page.link/app'.format(
                name, deal_id, phone_number, call_type),
            from_='whatsapp:+13614203347',
            to='whatsapp:{}'.format(mobileNumber)
        )

        print(message.sid)

    # response = requests.post(
    #     "https://adb-144004930359983.3.azuredatabricks.net/api/2.0/jobs/run-now", data=json.dumps({
    #         "job_id": 1646,
    #         "notebook_params": {
    #             "database": dbName,
    #         }
    #     }), headers={
    #         "Authorization": "Bearer dapi0df01b87c4a9b04e09e337b0aa08b6e7",
    #         "Content-Type": "application/json"
    #     })

    # print(response)

    return func.HttpResponse(
        "Success",
        status_code=200
    )
    # else:
    #     return func.HttpResponse("User not found", status_code=401)
