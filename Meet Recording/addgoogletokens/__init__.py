import logging
import psycopg2
import jwt
import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    req_body = req.get_json()
    accessToken = req_body.get('access')
    email = req_body.get('email')
    refreshToken = req_body.get('refresh')
    useremail = req_body.get('email')
    userId = req_body.get('userId')

    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    # conn = createDBConnection('auth', '')
    # cursor = conn.cursor()

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE email='{}';".format(email))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     dbName = records[0]
    # cursor.close()
    # conn.close()

    # dbName = 'expo_prod'
    conn = createDBConnection('', dbName)
    cursor = conn.cursor()

    cursor.execute(
        "SELECT access_token, refresh_token FROM google_oauth_tokens WHERE user_id = '" + userId + "';")

    if cursor.rowcount == 0:
        cursor.execute("INSERT INTO google_oauth_tokens(user_id, access_token, refresh_token, email_id) VALUES('" +
                       userId + "', '" + accessToken + "', '" + refreshToken + "', '" + useremail + "');")
    else:
        cursor.execute(
            "UPDATE google_oauth_tokens SET access_token='" + accessToken + "', refresh_token='" + refreshToken + "', email_id='" + useremail + "' WHERE user_id='" + userId + "'")

    conn.commit()

    cursor.close()
    conn.close()

    return func.HttpResponse("Token is set", status_code=200)
    # else:
    #     return func.HttpResponse("User not found", status_code=401)
