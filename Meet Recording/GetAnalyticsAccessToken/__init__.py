import logging
import jwt
import json
from datetime import datetime, timedelta
from cryptography.hazmat.primitives import serialization
import requests
import azure.functions as func


def getParameterByName(param, request):
    returnVal = None
    try:
        returnVal = request.params.get(param)
    except:
        print('Value not provided for ' + param)
    finally:
        return returnVal


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    intentName = getParameterByName('intent', req)
    apiMode = getParameterByName('env', req) or "prod"

    config = {
        "google_auth_url": "https://oauth2.googleapis.com/token"
    }

    private_key = b"-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCkSB9vO/GGAehj\nwyKkHqJkx6snMsgsPA3qJCWx4InjDsk26Jg0LnKcsQATcq70lViTnU85QJCyl5fl\nFTV1bWIu1SV9+pqlPgB6gVbz0iUxnVodb0W3W19KMADG3YVWhEh1I38WhE+ICZ1Q\ndXOBJD4S9riLElRqmzf5rPSZcwL6srQifIgNRE+tR824n017fpTlVPh070XFD/rS\nKmQmSy9Soms9twtWAal9THeuedbaept++Z7K/YAah0PU0/tQeRJv+c/TGCBmbgH8\nMZDZTtsObFS7jzk19+QT4jVOLazXa2TewKFWqSpNH1sTY8Zk0prIXLF0YBdOPij1\n771SZR+nAgMBAAECggEACZEbp4YloAivaFPeShFwoo7QJEW8S3sp0kdNnKi3Tu43\nAIgWCP/lc8OI/hjxIFQsJtiD2xEr/AKA+Xm9Nbvrek+2UbpM1C+ipn+uuR81REjm\nDQ3EURO0nmTiPrJ6x9mHQK8TARQub30j2KImWlBqfv84Kv0jxluIrHUKf38nnsK+\nYiZNKJbvzJFjQkLFjJO8aWvWPGz4s/WB5FZbBcHBRFpDR4k6BdlsPNxA7UoWGxDD\nEHEZTYNyajcU8fA5D4PcdVfc1j+V0f9KBNwtZQP4wZ+y9692gqKylxfRmNWXCkYd\nE6Bt2IPuAOSs1UmN5/oe0K3n/xFAap9TEBCEWH1cIQKBgQDix34xi5oHkIYuDrJz\nZKr80zOycXUF7xWqkJjPbB/3WGrcUgCRtPH/bbkN8ziI2BvQ7ON5c7semWnvvXOy\nlhfkUo/dWRXJk56V5obZgZU/H4JhB1kFqi4UdUr65VIzmMhwJT1fp76L8hh0vHaD\njWO5Nk40ijO7ATJLPKu5PDCrYQKBgQC5cxhpBvDr4YPBsgocPMXpOUC43Zk6+jgI\n1FEhFCJc5WvNcz3mY8GdOuiDwSgw/Pq8rAjQjDvHUQ3SqiEytfUwC+KbXpRtuYxg\no1IXFRfEaG1Zmhb/UeMEUpkxtnyRXjg2cbTLfz1bZBIE9no19er84mZyp1kI9045\n/uuDf9ZwBwKBgBi1zg1N/dGXMNex6EXkkTENPlPSQwMLgxvbt5raWD1WWAZ7tO4r\nfjbr2L8pYl6e1aEVPtAMCTaTVtS1mva5ng7V5DElNNeLCzugK89lbEK9hzJVX6Db\nYzv9b9cYx7Uxj52QoThcCcor7fhCVNOOw2/599mHuY1F2xoRmVCMu2zhAoGAMlCU\n3eIBOa5c5g+dCJ5AgA+FjOKqsUK0M0kPne7th9DDF+HdeIW2QSd7cEvW0LvCd7Ah\nUBvMGD7YhkCcczrFBD0lRUw67y45dPq6ygJyCVrf2ECjXrBj7IdQgxlk4Lf0fmkJ\nSXEDP6l97hTFgaBwGh5+9zqKQJGAiuGsGmhz0L8CgYBuUSXe4DHcLW15V4N11bkH\nRJBOmNaXqw8jZjEh9ePsXSl5GDOzvnmtTRpz3AQyrGuxfEa6H4QXed4QsLKgx0ti\n3lo7kRfqpWxSbk/Sn/mSOFMTa0zFTOCznTGz1uFxlSAC9AkGGT2GKB28CN7lSPRW\n8If2uhBN8JCSeIwgGEBaDQ==\n-----END PRIVATE KEY-----\n"

    header = {
        "alg": "RS256",
        "typ": "JWT",
        "kid": "e6d04917e597d0b87b0574b07eb2e2b83d10865e"
    }

    data = {
        "iss": "analyticsaccount@analytics-embedded.iam.gserviceaccount.com",
        "scope": "https://www.googleapis.com/auth/analytics.readonly",
        "aud": "https://oauth2.googleapis.com/token",
        "iat": datetime.now().timestamp(),
        "exp": (datetime.now() + timedelta(hours=1)).timestamp()
    }

    loaded_private_key = serialization.load_pem_private_key(
        private_key, password=None)

    # Encrypt the data using the public key.
    ciphertext = jwt.encode(
        data, loaded_private_key, algorithm="RS256", headers=header)

    authBody = {
        "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
        "assertion": ciphertext
    }

    response = requests.post(config['google_auth_url'], data=authBody)

    if(response.status_code == 200):
        responseBody = response.json()
        return func.HttpResponse(json.dumps(responseBody), status_code=200)
    else:
        return func.HttpResponse("Error", status_code=500)
