import logging
import psycopg2
import json
import pandas as pd
import requests
import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userId = req.params.get('id')

    # conn = createDBConnection('auth', '')
    # cursor = conn.cursor()

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE emp_id='{}';".format(userId))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchall()
    #     dbName = records[0][0]
    #     cursor.close()
    #     conn.close()
    dbName = 'expo_prod'

    conn = createDBConnection('', dbName)
    cursor = conn.cursor()

    cursor.execute(
        "SELECT access_token, refresh_token FROM google_oauth_tokens WHERE user_id = '" + userId + "';")

    if cursor.rowcount > 0:
        records = cursor.fetchall()
        accessToken = records[0][0]
        refreshToken = records[0][1]
        resp = requests.get(
            "https://www.googleapis.com/calendar/v3/calendars/primary/events", headers={"Authorization": "Bearer " + accessToken})

        if(resp.status_code == 200):
            status = 200
        else:
            # if Access token has expired fetch new access token
            clientId = '654851680174-p46gqlq2jgdgvjpag7n0p036duu5gm44.apps.googleusercontent.com'
            data = {
                'client_id': clientId,
                'refresh_token': refreshToken,
                'grant_type': 'refresh_token',
                'enablePKCE': False
            }
            tokenResp = requests.post(
                "https://oauth2.googleapis.com/token", data)

            if(tokenResp.status_code == 200):
                jsonData = json.loads(tokenResp.text)
                cursor.execute("UPDATE google_oauth_tokens SET access_token = '" +
                               jsonData['access_token'] + "' WHERE user_id='" + userId + "';")
                conn.commit()
                status = 200
            else:
                # Refresh token for the user has expired
                status = 204
    else:
        # User is not present in the database
        status = 204

    cursor.close()
    conn.close()

    if(status == 204):
        return func.HttpResponse("User not found", status_code=204)
    elif(status == 200):
        return func.HttpResponse("Token is available", status_code=200)
    # else:
    #     return func.HttpResponse("User not found", status_code=401)
