import json
import logging
import jwt
import psycopg2
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    decodedToken = verify_auth(str(req.headers.get('Authorization')))
    tenant = decodedToken['tenantid']

    logging.info(f'Searching pipeline for {tenant}')

    conn = createDBConnection("Authentication")
    cursor = conn.cursor()

    cursor.execute(
        f"SELECT pipeline FROM central.tenant_crm_credential WHERE tenant_name = '{tenant}'; ")

    logging.info(f'Total rows {cursor.rowcount} found')

    final = {}
    if(cursor.rowcount > 0):
        records = cursor.fetchone()
        logging.info(f'Data Found')
        logging.info(records[0])
        final['pipeline'] = records[0]

    return func.HttpResponse(
        json.dumps(final),
        status_code=200
    )
