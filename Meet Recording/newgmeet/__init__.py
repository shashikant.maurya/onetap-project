import logging
import psycopg2
import json
import math
import uuid
import requests
import os
from urllib.parse import urlparse, quote
import azure.functions as func


def createDBConnection(dbname):

    if(dbname == 'auth'):
        dbname = "Authentication"

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getParameterByName(param, request):
    returnVal = ""
    try:
        returnVal = request.params.get(param)
    except:
        print('Value not provided for ' + param)
    finally:
        return returnVal


def make_tiny(url):
    a = urlparse(url)
    tempName = os.path.basename(a.path)
    return url.replace(tempName, quote(tempName))
    # print(a.path)                    # Output: /kyle/09-09-201315-47-571378756077.jpg
    # print(os.path.basename(a.path))  # Output: 09-09-201315-47-571378756077.jpg

    # reqData = {
    #     "url": url,
    #     "domain": "tiny.one",
    #     "alias": str(uuid.uuid4())[:8],
    #     "tags": ""
    # }
    # response = requests.post("https://api.tinyurl.com/create", data=reqData, headers={
    #     'Authorization': 'Bearer K66XRQoeasXbPmzfXp6jZj9fbQvRcsiXKGnJTljfSpQUtEYhg3PByFKrOIcn'})
    # return json.loads(response.text)["data"]["tiny_url"]


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userId = getParameterByName('id', req)
    pageNo = getParameterByName('pageNo', req)

    if pageNo == "" or pageNo == None:
        pageNo = 1
    pageNo = int(pageNo)

    row_count = 5
    row_to_skip = (pageNo - 1) * row_count
    # leadId = getParameterByName('leadId', req)
    # startTime = getParameterByName('start', req)
    # endTime = getParameterByName('end', req)

    # print(f"Fetching data for user {userId}")
    # conn = createDBConnection('auth')
    # cursor = conn.cursor()

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE emp_id='{}';".format(userId))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchall()
    #     dbName = records[0][0]
    #     cursor.close()
    #     conn.close()

    dbName = 'expo_prod'

    conn = createDBConnection(dbName)
    cursor = conn.cursor()

    # whereCondition = "leadowner_id = '" + userId + "'"
    # if leadId != "" and leadId != None:
    #     whereCondition = whereCondition + \
    #         " and lead_id = '{}'".format(leadId)
    # elif startTime != "" and startTime != None and endTime != "" and endTime != None:
    #     whereCondition = whereCondition + \
    #         " and created_date BETWEEN TO_DATE('{}','YYYY-MM-DD') and TO_DATE('{}','YYYY-MM-DD')".format(
    #             startTime, endTime)
    # elif startTime != "" and startTime != None:
    #     whereCondition = whereCondition + \
    #         " and created_date = TO_DATE('{}', 'YYYY-MM-DD')".format(startTime)

    # whereCondition = whereCondition + " ORDER BY created_date DESC LIMIT 1;"

    # cursor.execute("SELECT e.email, recording_id, meeting_id, title, description, recording_url, participants, mr.created_date, mr.created_by, created, deal_id, deal_stage, meeting_datetime, meeting_duration, meeting_link, blob_path, converted_blob_path FROM meet_recordings mr JOIN employee e ON mr.created_by = e.emp_id WHERE mr.created_by = '{}' and (deal_id IS NULL or deal_id = '') and (deal_stage IS NULL or deal_stage = '');".format(userId))
    cursor.execute("SELECT e.email, recording_id, title, description, participants, meeting_datetime, blob_path, count(*) OVER() AS total_rows FROM meet_recordings mr JOIN employee e ON mr.created_by = e.emp_id AND mr.created_by = '{}' WHERE deal_id IS NULL or deal_id = '' ORDER BY recording_id DESC LIMIT {} OFFSET {};".format(userId, row_count, row_to_skip))

    # columns = [column[0] for column in cursor.description]  # 2
    # pdf = pd.DataFrame(columns=columns)  # 4
    total_pages = 0
    gmeetList = []

    if cursor.rowcount > 0:
        records = cursor.fetchall()

        total_pages = math.ceil(records[0][7] / row_count)

        dealWhereClause = "where user_email = '{}'".format(records[0][0])
        cursor.execute(
            "SELECT id, deal_name, current_lead_stage, amount, sales_account, contact_email FROM expo_deals_table {}".format(dealWhereClause))

        dealRecords = cursor.fetchall()
        for i in range(0, len(records)):

            dealList = []
            participants = records[i][4]
            # participants = set(participants)
            if cursor.rowcount > 0:
                for x in range(0, len(dealRecords)):
                    if dealRecords[x][5] in participants:
                        tempDeal = {
                            "id": str(dealRecords[x][0]),
                            "name": str(dealRecords[x][1]),
                            "stage": str(dealRecords[x][2]),
                            "amount": str(dealRecords[x][3]),
                            "sales_account": str(dealRecords[x][4])
                        }
                        dealList.append(tempDeal)
                        continue

            # dealList = searchDealList(
            #     records[i][0], records[i][4], cursor)
            tempGmeet = {
                "recording_id": records[i][1],
                "title": records[i][2],
                "description": records[i][3],
                "participants": records[i][4],
                "meeting_datetime": records[i][5],
                "blob_path": make_tiny(records[i][6]),
                "deals": dealList
            }
            gmeetList.append(tempGmeet)

    # for i in range(0, len(columns)):
    #     temp = []
    #     for j in records:
    #         temp.append(j[i])
    #     pdf[str(columns[i])] = temp

    # result = pdf.to_json(orient='records')
    # meetRecordingList = json.loads(result)

    # for i in range(0, len(meetRecordingList)):
    #     meetItem = meetRecordingList[i]

    # final = json.dumps(meetRecordingList)
    # final = json.dumps(gmeetList)

    cursor.close()
    conn.close()

    jsonData = {
        "total_pages": total_pages,
        "meet_list": gmeetList
    }

    return func.HttpResponse(json.dumps(jsonData))
    # else:
    #     return func.HttpResponse("User not found", status_code=401)


# def searchDealList(ownerEmail, participants, cursor):

#     return dealList
