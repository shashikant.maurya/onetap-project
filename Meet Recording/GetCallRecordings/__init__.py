import logging
import psycopg2
import json
import jwt
import os
from datetime import datetime, timedelta
from psycopg2.extras import RealDictCursor
import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getParameterByName(param, request):
    return request.params.get(param)


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key=os.environ['JWT_Secret'], algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    userId = getParameterByName('id', req)
    leadId = getParameterByName('leadId', req)
    startTime = getParameterByName('start', req)
    endTime = getParameterByName('end', req)
    searchQuery = getParameterByName('q', req)
    dbName = 'expo_prod'

    if(userId == None):
        return func.HttpResponse("No userId value is passed", status_code=500)

    print(f"Fetching data for user {userId}")
    # conn = createDBConnection('auth', '')
    # cursor = conn.cursor()

    # cursor.execute(
    #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE emp_id='{}';".format(userId))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchall()
    #     dbName = records[0][0]

    conn = createDBConnection('', dbName)
    cursor = conn.cursor(cursor_factory=RealDictCursor)

    whereCondition = "leadowner_id = '" + userId + "'"

    if leadId != None:
        whereCondition = whereCondition + \
            " and lead_id = '{}'".format(leadId)

    if startTime != None and endTime != None:
        whereCondition = whereCondition + \
            " and created_date BETWEEN '{}' and '{}'".format(
                startTime, endTime)

    if startTime != None and endTime == None:
        dateStr = startTime.split("-")
        start = datetime(int(dateStr[0]), int(
            dateStr[1]), int(dateStr[2]), 0, 0, 0, 0)
        whereCondition = whereCondition + \
            " and created_date > '{}' and created_date < '{}'".format(
                start, start + timedelta(days=1))

    if searchQuery != None:
        whereCondition = whereCondition + \
            " and name ILIKE '%{}%' OR notes ILIKE '%{}%' OR summary ILIKE '%{}%' OR phone_number ILIKE '%{}%'".format(
                searchQuery, searchQuery, searchQuery, searchQuery)

    whereCondition = whereCondition + " ORDER BY created_date DESC LIMIT 10;"

    cursor.execute("SELECT name, phone_number, call_type, recording_link, notes, deal_id, to_char(created_date, 'YYYY-MM-DD') created_date, start_time, end_time, leadowner_id, workspaceid, summary, stage, account_name FROM call_recording WHERE {}".format(whereCondition))

    records = cursor.fetchall()

    final = json.dumps(records)

    cursor.close()
    conn.close()

    return func.HttpResponse(final)
    # else:
    #     return func.HttpResponse("User not found", status_code=401)
