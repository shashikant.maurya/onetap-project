import json
import logging

import azure.functions as func
import jwt
import psycopg2


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getValue(val):
    return '' if val == None else val


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def Convert(tup, di):
    for a, b in tup:
        di.setdefault(a, b)
    return di


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    result = {}
    dbName = ""
    email = req.params.get('email')

    # conn = createDBConnection('Authentication')
    # cursor = conn.cursor()

    # cursor.execute(f"""
    #     SELECT
    #         database_name
    #     FROM
    #         central.employee e
    #             JOIN
    #         central.workspace w ON e.workspaceid = w.name
    #     WHERE e.email = '{email}';
    # """)

    # # cursor.execute(
    # #     "SELECT w.database_name FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE email='{}';".format(email))

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     dbName = records[0]
    # cursor.close()
    # conn.close()

    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    conn = createDBConnection(dbName)
    cursor = conn.cursor()

    cursor.execute("""
    SELECT
        x.stage, CASE WHEN y.stage_count IS NULL THEN 0 ELSE y.stage_count END total
    FROM
        ((SELECT
                system_stage stage
            FROM
                expo_deals_table
            WHERE
                system_stage NOT IN ('Won', 'Lost'))
        UNION
        (SELECT unnest('{{Won,Lost}}'::text[]) row1)) x
            LEFT JOIN
        (SELECT
            edt.system_stage stage, COUNT(edt.system_stage) filter (where edt.system_stage = edt.system_stage) stage_count
        FROM
            expo_deals_table edt
                JOIN
            freshsales_credentials fc ON edt.owner_id = CAST(fc.freshsales_id as BIGINT)
                JOIN
            employee e ON fc.leadownerid = e.emp_id
        WHERE
            email = '{}'
        GROUP BY edt.system_stage) y ON y.stage = x.stage
    ORDER BY CASE WHEN y.stage_count IS NULL THEN 0 ELSE y.stage_count END DESC;
    """.format(email))

    # cursor.execute("""
    # SELECT
    #     x.stage, CASE WHEN y.stage_count IS NULL THEN 0 ELSE y.stage_count END total
    # FROM
    #     ((SELECT
    #             current_lead_stage as stage
    #         FROM
    #             expo_deals_table
    #         WHERE
    #             current_lead_stage NOT IN ('Won', 'Lost'))
    #     UNION
    #     (SELECT unnest('{{Won,Lost}}'::text[]) row1)) x
    #         LEFT JOIN
    #     (SELECT
    #         edt.current_lead_stage stage, COUNT(edt.current_lead_stage) filter (where edt.current_lead_stage = edt.current_lead_stage) stage_count
    #     FROM
    #         expo_deals_table edt
    #             JOIN
    #         freshsales_credentials fc ON edt.owner_id = CAST(fc.freshsales_id as BIGINT)
    #             JOIN
    #         employee e ON fc.leadownerid = e.emp_id
    #     WHERE
    #         email = '{}'
    #     GROUP BY edt.current_lead_stage) y ON y.stage = x.stage
    # ORDER BY CASE WHEN y.stage_count IS NULL THEN 0 ELSE y.stage_count END DESC;
    # """.format(email))

    total = 0
    keyOrder = ['Active Leads', 'Won', 'Prospect', 'Goal Identified', 'Proposal Sent', 'Proposal WIP', 'Pitch', 'Servicing', 'Warming Up', 'Initiation',
                'Visit', 'Negotiation', 'Commit/Paperwork', 'Commit', 'Product demo', 'POC WIP', 'POC Done', 'Cold', 'Lost', 'Deleted', 'Closed']
    notCount = ['Closed', 'Lost', 'Won', 'Deleted']
    if cursor.rowcount > 0:
        records = cursor.fetchall()
        for row in records:
            stage, count = row
            result[stage] = count
            if stage not in notCount:
                total = total + count

    result['Active Leads'] = total

    returnValue = Convert(
        sorted(result.items(), key=lambda i: keyOrder.index(i[0])), {})

    cursor.close()
    conn.close()

    return func.HttpResponse(
        json.dumps(returnValue),
        status_code=200
    )
