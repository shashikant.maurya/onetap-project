import datetime
import logging
import io
from googleapiclient.http import MediaIoBaseDownload
import psycopg2
from googleapiclient.discovery import build
from google.oauth2.credentials import Credentials
from datetime import datetime, timedelta, timezone
import requests
import json
from azure.storage.blob import BlobServiceClient, __version__, BlobBlock, BlobProperties, ContentSettings
import uuid
from twilio.rest import Client

import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def updateToken(conn, cursor, userId):

    status = 500
    userEmail = ''
    accessToken = ''
    refreshToken = ''

    cursor.execute(
        "SELECT email_id, access_token, refresh_token FROM google_oauth_tokens WHERE user_id = '" + userId + "';")

    if cursor.rowcount > 0:
        records = cursor.fetchall()
        userEmail = records[0][0]
        accessToken = records[0][1]
        refreshToken = records[0][2]
        resp = requests.get(
            "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + accessToken, headers={"Authorization": "Bearer " + accessToken})

        if(resp.status_code == 200):
            status = 200
        else:
            # if Access token has expired fetch new access token
            clientId = '605586451222-tdl91au87jnjt3on5vujce7juakis4b4.apps.googleusercontent.com'
            data = {
                'client_id': clientId,
                'refresh_token': refreshToken,
                'grant_type': 'refresh_token',
                'enablePKCE': False
            }
            tokenResp = requests.post(
                "https://oauth2.googleapis.com/token", data)

            if(tokenResp.status_code == 200):
                jsonData = json.loads(tokenResp.text)
                accessToken = jsonData['access_token']

                cursor.execute("UPDATE google_oauth_tokens SET access_token = '" +
                               jsonData['access_token'] + "' WHERE user_id ='" + userId + "';")
                conn.commit()
                status = 200
            else:
                # Refresh token for the user has expired
                status = 204
    else:
        # User is not present in the database
        status = 204

    finalData = {
        'status': status,
        'email': userEmail,
        'accessToken': accessToken,
        'refreshToken': refreshToken
    }
    return finalData


def driveDownload(file_id, accessToken, dbName, empId):
    # 'expo_0001_Meet-Recording_2021-11-08 13:30:31.amr'

    filePath = ''
    print("Azure Blob Storage v" + __version__)
    # Credentials.id_token = accessToken
    creds = Credentials(token=accessToken)

    upload_file_path = '{}/Meet-Recording/{}_{}_Meet-Recording_{}.mp4'.format(
        dbName.replace('_prod', ''), dbName.replace('_prod', ''), empId, (datetime.now()).replace(microsecond=0))

    print(upload_file_path)

    # upload_file_path = 'temp/{}.mp4'.format(file_id)
    local_file_name = '{}.mp4'.format(file_id)

    connect_str = 'DefaultEndpointsProtocol=https;AccountName=nudgeazurefuncstorage;AccountKey=//m35hRuUsQwpAFtMcQ5SKj6dwsBgfpXMkXWX2S7twX7q6fpBCwuYbC3HE5KWl6xZucrf6eigMbNug4okDu0sA==;EndpointSuffix=core.windows.net'
    container_name = 'audio-files'

    # Create a blob client using the local file name as the name for the blob
    blob_client = BlobServiceClient.from_connection_string(connect_str).get_blob_client(
        container=container_name, blob=upload_file_path)

    print("\nUploading to Azure Storage as blob:\n\t" + upload_file_path)

    service = build('drive', 'v3', credentials=creds, cache_discovery=False)

    # Call the Drive v3 API
    # results = service.files().list(
    #     pageSize=10, fields="nextPageToken, files(id, name)").execute()
    # items = results.get('files', [])

    # upload 64 MB for each request
    CHUNK_SIZE = 64*1024*1024

    request = service.files().get_media(fileId=file_id)
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        print("Download .".format(status.progress() * 100))

    # print(math.ceil(downloader._total_size/CHUNK_SIZE))

    # fh.seek(0)
    # with open(local_file_name, 'wb') as f:
    #     f.write(fh.read())
    #     f.close()

    # print("File downloaded locally")
    if blob_client.exists():
        blob_client.delete_blob()

    index = 0
    chunk_size = 0
    fetch_size = 0
    total_size = downloader._total_size
    block_list = []
    fh.seek(0)
    while chunk_size < downloader._total_size:
        if total_size > CHUNK_SIZE or total_size == CHUNK_SIZE:
            chunk_size = chunk_size + CHUNK_SIZE
            total_size = total_size - CHUNK_SIZE
            fetch_size = CHUNK_SIZE
        else:
            chunk_size = chunk_size + total_size
            fetch_size = total_size
            total_size = total_size - total_size

        read_data = fh.read(fetch_size)

        # Read data in chunks to avoid loading all into memory at once
        # for chunk in stream.chunks():
        # process your data (anything can be done here really. `chunk` is a byte array).
        block_id = str(uuid.uuid4())
        blob_client.stage_block(
            block_id=block_id, data=read_data)
        block_list.append(BlobBlock(block_id=block_id))
        index = index + 1
        logging.info(
            "Index = {} and total uploaded file size = {}".format(index, total_size))

    # Upload the whole chunk to azure storage and make up one blob
    blob_client.commit_block_list(block_list)
    blob_client.set_http_headers(ContentSettings(content_type='video/mp4'))

    filePath = "https://nudgeazurefuncstorage.blob.core.windows.net/{}/{}".format(
        container_name,  upload_file_path)

    return filePath


def make_tiny(url):
    reqData = {
        "url": url,
        "domain": "tiny.one",
        "alias": str(uuid.uuid4())[:8],
        "tags": ""
    }
    response = requests.post("https://api.tinyurl.com/create", data=reqData, headers={
        'Authorization': 'Bearer K66XRQoeasXbPmzfXp6jZj9fbQvRcsiXKGnJTljfSpQUtEYhg3PByFKrOIcn'})
    return json.loads(response.text)["data"]["tiny_url"]


def main(mytimer: func.TimerRequest) -> None:
    utc_timestamp = datetime.utcnow().replace(
        tzinfo=timezone.utc).isoformat()

    if mytimer.past_due:
        logging.info('The timer is past due!')

    logging.info('Python timer trigger function ran at %s', utc_timestamp)

    prodEnvironment = False

    if(prodEnvironment):
        conn = createDBConnection('auth', '')
        cursor = conn.cursor()

        cursor.execute(
            'SELECT DISTINCT database_name FROM central.workspace WHERE status = \'ready\' and is_active = 1 and database_name = \'expo_prod\';')

        if cursor.rowcount > 0:
            authRecords = cursor.fetchall()
            cursor.close()
            conn.close()
            for x in range(0, len(authRecords)):
                dbName = authRecords[x][0]

                conn = createDBConnection('', dbName)
                cursor = conn.cursor()

                cursor.execute(
                    'SELECT EXISTS(SELECT 1 FROM information_schema.tables WHERE upper(table_name)=upper(\'google_oauth_tokens\'))')
                records = cursor.fetchall()

                if records[0][0]:
                    print('Fetching data from {} database'.format(dbName))

                    cursor.execute(
                        """SELECT
                        e.emp_id, email_id, access_token, refresh_token
                            FROM
                        google_oauth_tokens got JOIN employee e ON e.emp_id=got.user_id
                            """)
                    if cursor.rowcount > 0:
                        tokenRecords = cursor.fetchall()
                        for x in range(0, len(tokenRecords)):
                            userId = tokenRecords[x][0]
                            userEmail = tokenRecords[x][1]
                            accessToken = tokenRecords[x][2]

                            conn1 = createDBConnection('auth', '')
                            cursor1 = conn1.cursor()
                            cursor1.execute(
                                'SELECT emp_name, email, mobile_number FROM central.employee WHERE email=\'{}\''.format(userEmail))

                            if cursor1.rowcount > 0:
                                records1 = cursor1.fetchall()
                                mobileNumber = records1[0][2]

                                data = updateToken(conn, cursor, userId)
                                accessToken = data['accessToken']

                                n = datetime.now()
                                timeMax = n.replace(microsecond=0)
                                timeMin = (n-timedelta(days=1)
                                           ).replace(microsecond=0)

                                meetingResponse = requests.get(
                                    "https://www.googleapis.com/calendar/v3/calendars/primary/events?timeMax={}Z&timeMin={}Z&orderBy=updated".format(str(timeMax).replace(" ", "T"), str(timeMin).replace(" ", "T")), headers={"Authorization": "Bearer " + accessToken})
                                # meetingResponse = requests.get(
                                #     "https://www.googleapis.com/calendar/v3/calendars/primary/events?orderBy=updated", headers={"Authorization": "Bearer " + accessToken})
                                if meetingResponse.status_code == 200:
                                    print("Access token is valid")

                                    meetingJson = json.loads(
                                        meetingResponse.text)
                                    for i in range(0, len(meetingJson["items"])):
                                        if 'attachments' in meetingJson["items"][i]:

                                            meeting_id = meetingJson["items"][i]["id"]

                                            cursor.execute(
                                                "SELECT meeting_id FROM meet_recordings WHERE meeting_id = '{}';".format(meeting_id))

                                            if cursor.rowcount == 0:
                                                # if True:

                                                blob_path = ''

                                                blob_path = driveDownload(
                                                    meetingJson["items"][i]["attachments"][0]['fileId'], accessToken, dbName, userId)

                                                meeting_date = meetingJson["items"][i]["created"]
                                                meeting_duration = int((datetime.strptime(meetingJson["items"][i]['end']['dateTime'], '%Y-%m-%dT%H:%M:%S%z') - datetime.strptime(
                                                    meetingJson["items"][i]['start']['dateTime'], '%Y-%m-%dT%H:%M:%S%z')).total_seconds())
                                                meeting_link = meetingJson["items"][i]["hangoutLink"]
                                                title = meetingJson["items"][i]["summary"]
                                                if 'description' in meetingJson["items"][i]:
                                                    description = meetingJson["items"][i]["description"]
                                                else:
                                                    description = ''
                                                recording_url = meetingJson["items"][i]["attachments"][0]["fileUrl"]
                                                participants = []

                                                dealWhereClause = ''
                                                for j in range(0, len(meetingJson["items"][i]["attendees"])):
                                                    participantEmail = meetingJson["items"][i]["attendees"][j]['email']
                                                    if j == 0:
                                                        dealWhereClause = dealWhereClause + \
                                                            ' where contact_email = \'{}\''.format(
                                                                participantEmail)
                                                    else:
                                                        dealWhereClause = dealWhereClause + \
                                                            ' or contact_email = \'{}\''.format(
                                                                participantEmail)
                                                    participants.append(
                                                        participantEmail)

                                                cursor.execute(
                                                    'SELECT id, deal_name, current_lead_stage, amount, sales_account FROM expo_deals_table {}'.format(dealWhereClause))

                                                deal_id = ''
                                                deal_name = ''
                                                deal_stage = ''
                                                deal_amount = ''
                                                deal_account = ''
                                                if cursor.rowcount == 1:
                                                    dealDetails = cursor.fetchall()
                                                    deal_id = dealDetails[0][0]
                                                    deal_name = dealDetails[0][1]
                                                    deal_stage = dealDetails[0][2]
                                                    deal_amount = dealDetails[0][3]
                                                    deal_account = dealDetails[0][4]

                                                created_by = userId
                                                created = datetime.strptime(
                                                    meetingJson["items"][i]['start']['dateTime'], '%Y-%m-%dT%H:%M:%S%z').strftime('%b-%y')

                                                cursor.execute(
                                                    "SELECT meeting_id FROM meet_recordings WHERE meeting_id = '{}';".format(meeting_id))

                                                logging.info(
                                                    "Blob Path = " + blob_path)

                                                if cursor.rowcount > 0:
                                                    cursor.execute("""UPDATE meet_recordings
                                                                                SET title='{}', description='{}', recording_url='{}', participants=ARRAY{}, created_by='{}', created='{}', meeting_datetime='{}', meeting_duration='{}', meeting_link='{}', blob_path='{}', deal_id='{}', deal_stage='{}'
                                                                                WHERE meeting_id='{}' """.format(title, description, recording_url, participants, created_by, created, meeting_date, meeting_duration, meeting_link, blob_path, deal_id, deal_stage, meeting_id))
                                                else:
                                                    cursor.execute("""INSERT INTO meet_recordings(
                                                                    meeting_id, title, description, recording_url, participants, created_by, created, meeting_datetime, meeting_duration, meeting_link, blob_path, deal_id, deal_stage)
                                                                    VALUES('{}', '{}', '{}', '{}', ARRAY{}, '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}');
                                                                        """.format(meeting_id, title, description, recording_url, participants, created_by, created, meeting_date, meeting_duration, meeting_link, blob_path, deal_id, deal_stage))
                                                conn.commit()

                                                # if mobileNumber == None:
                                                mobileNumber = '+917290932901'

                                                # Find your Account SID and Auth Token at twilio.com/console
                                                # and set the environment variables. See http://twil.io/secure
                                                # account_sid = os.environ['TWILIO_ACCOUNT_SID']
                                                account_sid = 'AC0b225129b1554a44382b4011f97ae3e8'
                                                # auth_token = os.environ['TWILIO_AUTH_TOKEN']
                                                auth_token = 'cffeb3b13ea13eb18ce9e93ade1f12b9'
                                                client = Client(
                                                    account_sid, auth_token)

                                                tinyurl = make_tiny(blob_path)

                                                if deal_id == '':
                                                    message = client.messages.create(
                                                        body="New Gmeet recordings are available:  \n If you would like to associate the recordings with a Deals type 'Assign Deal'.",
                                                        from_='whatsapp:+13614203347',
                                                        to='whatsapp:{}'.format(
                                                            mobileNumber)
                                                    )
                                                else:
                                                    message = client.messages.create(
                                                        body='A new gmeet recording is available: \nTitle: {} \nDate&Time: {} \nParticipants: {} \nDeal: {} \nAmount: {} \nAccount: {} \nStage: {} \n\nRecording Link: {}'.format(
                                                            title, datetime.strptime(
                                                                meetingJson["items"][i]['start']['dateTime'], '%Y-%m-%dT%H:%M:%S%z').strftime('%d %b %y %H:%M:%S'), ",".join(participants), deal_name, "Rs." + str(round(deal_amount, 2)), deal_account, deal_stage, tinyurl),
                                                        from_='whatsapp:+13614203347',
                                                        to='whatsapp:{}'.format(
                                                            mobileNumber)
                                                    )

                                                print(message.sid)
                                else:
                                    print("Access token is invalid or expired")

                        cursor.close()
                        conn.close()
                    else:
                        cursor.close()
                        conn.close()
