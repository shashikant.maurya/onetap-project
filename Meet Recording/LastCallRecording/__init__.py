import logging
import psycopg2
import json
import pandas as pd
import requests
import azure.functions as func


def createDBConnection(type, db):

    if(type == 'auth'):
        dbname = "Authentication"
    else:
        dbname = db

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getParameterByName(param, request):
    returnVal = ""
    try:
        returnVal = request.params.get(param)
    except:
        print('Value not provided for ' + param)
    finally:
        return returnVal


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    email = getParameterByName('email', req)
    userId = getParameterByName('id', req)
    leadId = getParameterByName('leadId', req)
    startTime = getParameterByName('start', req)
    endTime = getParameterByName('end', req)

    print(f"Fetching data for user {userId}")
    conn = createDBConnection('auth', '')
    cursor = conn.cursor()

    if email != "":
        cursor.execute(f"""
            SELECT 
                database_name, email
            FROM 
                central.employee e 
                    JOIN 
                central.workspace w ON e.workspaceid = w.name
            WHERE e.email = '{email}';
        """)
    else:
        cursor.execute(
            "SELECT w.database_name, e.email FROM central.employee e JOIN central.workspace w ON e.workspaceid=w.name WHERE emp_id='{}';".format(userId))

    if cursor.rowcount > 0:
        records = cursor.fetchone()
        dbName = records[0]
        email = records[1]
        cursor.close()
        conn.close()

        conn = createDBConnection('', dbName)
        cursor = conn.cursor()

        whereCondition = "created_by = '" + userId + "'"
        if leadId != "" and leadId != None:
            whereCondition = whereCondition + \
                " and lead_id = '{}'".format(leadId)
        elif startTime != "" and startTime != None and endTime != "" and endTime != None:
            whereCondition = whereCondition + \
                " and created_date BETWEEN TO_DATE('{}','YYYY-MM-DD') and TO_DATE('{}','YYYY-MM-DD')".format(
                    startTime, endTime)
        elif startTime != "" and startTime != None:
            whereCondition = whereCondition + \
                " and created_date = TO_DATE('{}', 'YYYY-MM-DD')".format(startTime)

        whereCondition = whereCondition + " ORDER BY created DESC LIMIT 1;"

        cursor.execute("SELECT recording_type call_type, name, phone_number, notes, blob_path recording_link, start_time, end_time, deal_id, deal_stage, account_name, created_by, created created_date FROM recording_master WHERE {}".format(whereCondition))

        # cursor.execute("SELECT name, phone_number, call_type, recording_link, notes, deal_id, created_date, start_time, end_time, leadowner_id, workspaceid, summary, stage, account_name FROM call_recording WHERE {}".format(whereCondition))

        columns = [column[0] for column in cursor.description]  # 2
        pdf = pd.DataFrame(columns=columns)  # 4

        records = cursor.fetchall()

        for i in range(0, len(columns)):
            temp = []
            for j in records:
                temp.append(j[i])
            pdf[str(columns[i])] = temp

        result = pdf.to_json(orient='records')
        myManagerLearning = json.loads(result)

        final = json.dumps(myManagerLearning)

        cursor.close()
        conn.close()

        return func.HttpResponse(final)
    else:
        return func.HttpResponse("User not found", status_code=401)
