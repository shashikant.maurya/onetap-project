import logging
import psycopg2
import json
import requests
import jwt
import azure.functions as func


def createDBConnection(dbname):

    host = "nudge.postgres.database.azure.com"
    user = "nudge"
    password = "Exponentia@27"
    sslmode = "require"

    # Construct connection string
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


def getValue(val):
    return '' if val == None else val


def verify_auth(authtoken):
    TOKEN_PREFIX = 'Bearer'
    bearer, _, token = authtoken.partition(' ')
    if(token == None or bearer != TOKEN_PREFIX):
        raise PermissionError(
            '{"errorMsg": "auth token not found or was invalid"}', 403)
    try:
        decodedToken = jwt.decode(
            jwt=token, key='MfwtvVmtrmv!_v1', algorithms=['HS256'])
        return decodedToken
    except Exception as e:
        raise PermissionError(
            '{"errorMsg": "%s"}' % e.args[0], 403)


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    result = "failed"
    dbName = ""
    id = req.params.get('id')
    email = req.params.get('email')
    action = req.params.get('action')

    if action.lower() == 'approve':
        req_body = req.get_json()
        title = getValue(req_body.get('title'))
        desc = getValue(req_body.get('description'))
        dueDate = getValue(req_body.get('due'))

    # conn = createDBConnection('Authentication')
    # cursor = conn.cursor()

    # cursor.execute(f"""
    #     SELECT
    #         database_name
    #     FROM
    #         central.employee e
    #             JOIN
    #         central.workspace w ON e.workspaceid = w.name
    #     WHERE e.email = '{email}';
    # """)

    # if cursor.rowcount > 0:
    #     records = cursor.fetchone()
    #     dbName = records[0]
    # cursor.close()
    # conn.close()
    decodedToken = verify_auth(str(req.headers.get('Authorization')))

    dbName = decodedToken['database_name']

    conn = createDBConnection(dbName)
    cursor = conn.cursor()

    if action.lower() == 'approve':
        cursor.execute("""
            SELECT
                fc.*,
                sot.dealid
            FROM
                employee e
                    JOIN
                freshsales_credentials fc ON e.emp_id = fc.leadownerid
                    JOIN
                sentiment_output_table sot ON fc.leadownerid = sot.leadownerid AND primary_id = {} AND action_taken = 0
            WHERE
                email = '{}';
        """.format(id, email))

        if cursor.rowcount > 0:
            records = cursor.fetchone()

            data = {
                "task": {
                    "title": title,
                    "description": desc,
                    "due_date": dueDate,
                    "owner_id": records[4],
                    "targetable_id": records[6],
                    "targetable_type": "Deal",
                    "task_users_attributes": [
                        {"user_id": records[4]}
                    ]
                }
            }

            requestStatus = requests.post('{}api/tasks'.format(records[1]), data=json.dumps(data), headers={
                'Content-Type': 'application/json; charset=UTF-8', 'Authorization': 'Token token=' + records[2]})

            if requestStatus.status_code == 201 or requestStatus.status_code == 200:
                cursor.execute("""
                    UPDATE
                        sentiment_output_table
                    SET
                        action_taken = 1
                    WHERE
                        primary_id = {};
                """.format(id))
                conn.commit()
                result = "success"
    else:
        cursor.execute("""
                UPDATE
                    sentiment_output_table
                SET
                    action_taken = 2
                WHERE
                    primary_id = {};
            """.format(id))
        conn.commit()
        result = "success"

    cursor.close()
    conn.close()

    return func.HttpResponse(
        result,
        status_code=200
    )
