const fs = require('fs');
const path = require('path');
const { Pool } = require('pg');
const { BlobServiceClient } = require("@azure/storage-blob");
const { v4: uuidv4 } = require('uuid');
const connStr = "DefaultEndpointsProtocol=https;AccountName=nudgeazurefuncstorage;AccountKey=//m35hRuUsQwpAFtMcQ5SKj6dwsBgfpXMkXWX2S7twX7q6fpBCwuYbC3HE5KWl6xZucrf6eigMbNug4okDu0sA==;EndpointSuffix=core.windows.net";
const blobServiceClient = BlobServiceClient.fromConnectionString(connStr);
const containerName = "audio-files";
const https = require('https');

async function getDBConnection(dbName) {
    return new Promise((resolve, reject) => {
        var conn;
        if (dbName == '')
            dbName = "Authentication"

        conn = new Pool({
            user: 'nudge',
            host: 'nudge.postgres.database.azure.com',
            database: dbName,
            password: 'Exponentia@27',
            port: 5432,
            ssl: true
        });

        resolve(conn);
    });
}

const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function executeCommand(cmd) {
    const { stdout, stderr } = await exec(cmd);
    console.log('stdout:', stdout);
    console.log('stderr:', stderr);
    return stdout;
}

async function downloadBlob(containerName, blobName, fileName) {
    try {
        return new Promise(async (resolve, reject) => {
            const containerClient = blobServiceClient.getContainerClient(containerName);
            const blobClient = containerClient.getBlobClient(blobName);

            // Get blob content from position 0 to the end
            // In Node.js, get downloaded data by accessing downloadBlockBlobResponse.readableStreamBody
            const downloadBlockBlobResponse = await blobClient.download();
            const downloaded = (
                await streamToBuffer(downloadBlockBlobResponse.readableStreamBody)
            );
            let writeStream = fs.createWriteStream(fileName);

            // write some data with a binary encoding
            writeStream.write(downloaded, 'binary');

            writeStream.on('error', function (err) {
                console.log(err);
                reject(err);
            });

            // the finish event is emitted when all data has been flushed from the stream
            writeStream.on('finish', () => {
                console.log('wrote all data to file');
                resolve("success");
            });

            // close the stream
            writeStream.end();
        }).catch((error) => {
            console.error(error);
        });
    } catch (e) {
        console.log(e.message);
    }
}

async function executeDBQuery(pool, query) {
    return new Promise((resolve, reject) => {
        pool.query(query, (err, res) => {
            if (err) reject(err);
            console.log(err, res)
            resolve(res);
        });
    });
}

async function convertFile(fileName, outFile) {
    return new Promise((resolve, reject) => {
        var spawn = require('child_process').spawn;

        var cmd = './FFMPEG/ffmpeg';

        var args = [
            '-i', fileName,
            '-acodec', 'pcm_s16le',
            '-ac', '1',
            '-ar', '16000',
            '-f', 'wav', '-y', outFile
        ];

        var proc = spawn(cmd, args);

        proc.stdout.on('data', function (data) {
            console.log(data);
        });

        proc.stderr.setEncoding("utf8")
        proc.stderr.on('data', function (data) {
            console.log(data);
        });

        proc.on('close', function () {
            console.log('finished');
            resolve("success");
        });

        proc.on('exit', function (code) {
            console.log(`child process exited with code ${code}`);
            if (code != 0) reject('error');
        });
    });
}

async function uploadFile(containerName, uploadPath, fileName) {
    return new Promise((resolve, reject) => {
        const containerClient = blobServiceClient.getContainerClient(containerName);

        const blockBlobClient = containerClient.getBlockBlobClient(uploadPath);
        blockBlobClient.setHTTPHeaders({ 'blobContentType': 'audio/wav' });

        blockBlobClient.uploadFile(fileName).then((resp) => {
            console.log(resp);
            resolve("success");
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
}

async function execute(command) {
    return new Promise(function (resolve, reject) {
        const { exec } = require('child_process');
        exec(command, function (error, standardOutput, standardError) {
            if (error) {
                reject();
            }
            if (standardError) {
                console.log(standardError);
                reject(standardError);
            }
            resolve(standardOutput);
        });
    }).catch((err) => {
        console.log(err + "err");
    })
}

// [Node.js only] A helper method used to read a Node.js readable stream into a Buffer
async function streamToBuffer(readableStream) {
    return new Promise((resolve, reject) => {
        const chunks = [];
        readableStream.on("data", (data) => {
            chunks.push(data instanceof Buffer ? data : Buffer.from(data));
        });
        readableStream.on("end", () => {
            resolve(Buffer.concat(chunks));
        });
        readableStream.on("error", reject);
    });
}

async function deleteFile(filename) {
    return new Promise((resolve, reject) => {
        fs.unlink(filename, (err) => {
            if (err) throw reject(err);
            resolve("success");
        });
    });
}

async function post(url, data, headers) {
    const dataString = JSON.stringify(data)

    const options = {
        method: 'POST',
        headers: headers,
        timeout: 1000, // in ms
    }

    return new Promise((resolve, reject) => {
        const req = https.request(url, options, (res) => {
            if (res.statusCode < 200 || res.statusCode > 299) {
                return reject(new Error(`HTTP status code ${res.statusCode}`))
            }

            const body = []
            res.on('data', (chunk) => body.push(chunk))
            res.on('end', () => {
                const resString = Buffer.concat(body).toString()
                resolve(resString)
            })
        })

        req.on('error', (err) => {
            reject(err)
        })

        req.on('timeout', () => {
            req.destroy()
            reject(new Error('Request time out'))
        })

        req.write(dataString)
        req.end()
    })
}

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    const fileId = req.body.id;
    const filePath = req.body.link;
    const dbName = req.body.db;
    const fileType = req.body.type;

    var pool = await getDBConnection(dbName);

    try {

        var uniqueId = uuidv4();
        context.log(fileId);
        context.log(filePath);
        var fileName = path.basename(filePath);
        var extension = path.extname(filePath).toLowerCase();
        var outFile = fileName.substring(0, fileName.lastIndexOf('.')) + '.wav';
        var blobPath = filePath.split(containerName)[1];
        var blobName = blobPath.substring(1, blobPath.length);
        // var tenantName = 

        // var duration = await executeCommand(`./FFMPEG/ffprobe -i ${filePath} -show_entries format=duration -v quiet -of csv="p=0"`);
        var duration = await execute(`.\\FFMPEG\\ffprobe.exe  -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ${filePath}`);

        context.log('Downloading File');
        var isdownload = await downloadBlob(containerName, decodeURIComponent(blobName), uniqueId + extension);

        context.log('File downloaded. Converting file');
        var isConverted = await convertFile(uniqueId + extension, uniqueId + "-converted.wav");

        context.log('File is converted successfully');
        if (isdownload == "success" && isConverted == "success") {
            console.log('Deleting the downloaded file');
            await deleteFile(uniqueId + extension);
        }

        var uploadFolder = '';
        if (fileType.toLowerCase() == 'in-person') {
            uploadFolder = "In-Person/"
        } else if (fileType.toLowerCase() == 'gmeet') {
            uploadFolder = "presales/Meet-Recording/"
        } else if (fileType.toLowerCase() == "outgoing-call" || fileType.toLowerCase() == "incoming-call") { 
            uploadFolder = "Call-Recording/"
        }

        context.log('Uploading the converted file');
        // var isUploaded = await uploadFile(containerName, (blobName.slice(0, blobName.lastIndexOf("/")) + blobName.slice(blobName.lastIndexOf("/"))).replace('original/', uploadFolder).replace(fileName, outFile), uniqueId + "-converted.wav");
        var isUploaded = await uploadFile(containerName, blobName.replace('original/', uploadFolder).replace(extension, '.wav').replace('.Wav', '.wav'), uniqueId + "-converted.wav");

        if (isUploaded == "success") {
            context.log('Converted file upload successfull and deleting the file');
            await deleteFile(uniqueId + "-converted.wav");
        }

        // convertedBlob = (filePath.slice(0, filePath.lastIndexOf("/")) + filePath.slice(filePath.lastIndexOf("/"))).replace(fileName, uniqueId + ".wav");
        convertedBlob = filePath.replace('original/', uploadFolder).replace(extension, '.wav').replace('.Wav','.wav');

        context.log('Updating the database');

        const query = {
            text: 'UPDATE recording_master SET name = $1, blob_path = $2, duration = $3, created = NOW() WHERE recording_id = $4;',
            values: [path.basename(convertedBlob), convertedBlob, duration, fileId],
        }

        temp = await executeDBQuery(pool, query);

        const getQuery = {
            text: 'SELECT * FROM recording_master WHERE recording_id = $1;',
            values: [fileId],
        }

        temp = await executeDBQuery(pool, getQuery);

        // if (temp.rowCount > 0) {

        //     // const homeScreenQuery = {
        //     //     text: 'INSERT INTO nudge.home_screen_nudges(module, leadownerid, header, description, module_link, card) VALUES($1, $2, $3, $4, $5, $6);',
        //     //     values: ['conversation', temp.rows[0].created_by, 'New Recording is available', 'A new in-person recording has been processed and scores are available.', '/conversation/deal/' + temp.rows[0].deal_id, '4']
        //     // }

        //     const homeScreenQuery = {
        //         text: 'INSERT INTO nudge.home_screen_nudges(module, leadownerid, header, description, module_link, image, card) VALUES($1, $2, $3, $4, $5, $6, $7);',
        //         values: ['conversation', temp.rows[0].created_by, 'New Recording', 'A new conversation is available.', '/conversation/deal/' + temp.rows[0].deal_id, 'https://nudgeazurefuncstorage.blob.core.windows.net/audio-files/cards/conversation-icon-png-transparent.png', '4']
        //     }

        //     await executeDBQuery(pool, homeScreenQuery);
        // }


        var data = {
            // "job_id": 1646,
            "job_id": 44100150270502,
            "notebook_params": {
                "database": dbName,
                "rec_id": fileId
            }
        }

        var headers = {
            'Content-Type': 'application/json',
            // 'Content-Length': dataString.length,
            // "Authorization": "Bearer dapi0df01b87c4a9b04e09e337b0aa08b6e7",
            "Authorization": "Bearer dapi968b2e3e2e395b7f0d75f662ce904b60",
        }

        // post("https://adb-144004930359983.3.azuredatabricks.net/api/2.0/jobs/run-now", data, headers);

        post("https://adb-8485776674143240.0.azuredatabricks.net/api/2.0/jobs/run-now", data, headers);


    } catch (error) {
        context.log(error);
    }
    // {
    //     "job_id": 1646,
    //         "notebook_params": {
    //         "database": dbName,

    //     }

    // }

    // var query = 'UPDATE recording_master SET blob_path = ';

    // conversionData = await executeDBQuery(pool, query);

    // const name = (req.query.name || (req.body && req.body.name));
    // const responseMessage = name
    //     ? "Hello, " + name + ". This HTTP triggered function executed successfully."
    //     : "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.";

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: "success"
    };
}