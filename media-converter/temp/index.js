const { BlobServiceClient } = require("@azure/storage-blob");
const {
    ShareClient,
    generateAccountSASQueryParameters,
    StorageSharedKeyCredential,
    AccountSASResourceTypes,
    AccountSASPermissions,
    AccountSASServices,
} = require("@azure/storage-file-share");

const fileAccountName = "";
const fileAccountKey = "";

const blobConStr = "";

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    generateSasToken("https://nudgeazurefuncstorage.blob.core.windows.net/audio-files/3470166902ce90843b852b4145e2162p.mp3", "//m35hRuUsQwpAFtMcQ5SKj6dwsBgfpXMkXWX2S7twX7q6fpBCwuYbC3HE5KWl6xZucrf6eigMbNug4okDu0sA==", )

    await copy();

    const name = (req.query.name || (req.body && req.body.name));
    const responseMessage = name
        ? "Hello, " + name + ". This HTTP triggered function executed successfully."
        : "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.";

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: responseMessage
    };
}

function generateSasToken(resourceUri, signingKey, policyName, expiresInMins) {
    resourceUri = encodeURIComponent(resourceUri);

    // Set expiration in seconds
    var expires = (Date.now() / 1000) + expiresInMins * 60;
    expires = Math.ceil(expires);
    var toSign = resourceUri + '\n' + expires;

    // Use crypto
    var hmac = crypto.createHmac('sha256', Buffer.from(signingKey, 'base64'));
    hmac.update(toSign);
    var base64UriEncoded = encodeURIComponent(hmac.digest('base64'));

    // Construct authorization string
    var token = "SharedAccessSignature sr=" + resourceUri + "&sig="
        + base64UriEncoded + "&se=" + expires;
    if (policyName) token += "&skn=" + policyName;
    return token;
};

async function copy() {
    // create account sas token for file service
    var fileCreds = new StorageSharedKeyCredential(
        fileAccountName,
        fileAccountKey
    );
    var accountSas = generateAccountSASQueryParameters(
        {
            startsOn: new Date(new Date().valueOf() - 8640),
            expiresOn: new Date(new Date().valueOf() + 86400000),
            resourceTypes: AccountSASResourceTypes.parse("sco").toString(),
            permissions: AccountSASPermissions.parse("rwdlc").toString(),
            services: AccountSASServices.parse("f").toString(),
        },
        fileCreds
    ).toString();

    //get file share client
    var shareClient = new ShareClient(
        `https://${fileAccountName}.file.core.windows.net/<shareName>`,
        fileCreds
    );
    //get blob container client
    var blobServiceClient = BlobServiceClient.fromConnectionString(blobConStr);
    var containerClient = blobServiceClient.getContainerClient("<containerName>");
    await containerClient.createIfNotExists();

    // list files and copy files to azure blob
    var arrFolders = [];
    arrFolders.push("input");
    do {
        let directoryName = arrFolders.pop();
        console.log(`List directories and files under directory ${directoryName}`);
        let i = 1;
        const directoryClient = shareClient.getDirectoryClient(directoryName);
        for await (const entity of directoryClient.listFilesAndDirectories()) {
            if (entity.kind === "directory") {
                console.log(`${i++} - directory\t: ${entity.name}`);
                arrFolders.push(
                    directoryName == "" ? entity.name : directoryName + "\\" + entity.name
                );
            } else {
                console.log(`${i++} - file\t: ${entity.name}`);
                var fileClient = directoryClient.getFileClient(entity.name);
                var soureUrl = fileClient.url + "?" + accountSas;
                try {
                    var res = await (
                        await containerClient
                            .getBlobClient(entity.name)
                            .beginCopyFromURL(soureUrl)
                    ).pollUntilDone();
                    console.log(res.copyStatus);
                } catch (error) {
                    throw error;
                }
            }
        }
    } while (arrFolders.length > 0);
}